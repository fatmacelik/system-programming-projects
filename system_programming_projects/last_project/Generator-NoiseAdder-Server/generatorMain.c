/*
 * 
 * GYTE 2014 BIL244 SYSTEM PROGRAMMING RETAKE
 * 
 * 
 * 20 HAZiRAN 2014 / CUMA / 23:50
 * 
 * FATMA CELiK  101044038
 * 
 * 
 * 
 * 
 * */
 
 
#include "generator.h"

int main(int argc, char *argv[])
{
	
    /* Handler ctrl + c */
    if( SetHandler( InterruptHandlerParent , SIGINT) == FAIL)
    {
        fprintf(stdout,"\n Failed to set signal handlers.\n\n");
        return FAIL;
    }
    
    /*-----------------------------------------------------------------*/
	
	if (checkArguman(argc,argv) == SUCCESS)
	{
		//printf("\n dir: %s  share: %s start: %s  end: %s \n\n",argv[2],argv[4],argv[6],argv[8]);		
		return Start(argv);
	}
	
	return 0;
}


/*
 * arguman kontrolu yapilir
 * */
int checkArguman(int argc, CHPTRPTR argv)
{
	CLEAR_SCREEN;
	DIRPTR dir;
	
	if(argc != 9)
	{
		usage();
		return FAIL;
	}
	
	if(strcmp(argv[0],"./Generator") != 0)
	{
		printf("\n Please, Check execution name!");
		usage();
		return FAIL;
	}

    
    if (argv[1] != NULL && !strcmp(argv[1], "-directory")) 
    {
        if (argv[2] == NULL) 
        {
            printf("\n Error :Invalid directory.\n\n");
            usage();
			return FAIL;
        }
        
        	dir = opendir(argv[2]);
			if(dir == NULL)
			{
				printf("\n Please, Check Directory Name!");
				printf("\n Directory is not opened!");
				usage();
				closedir(dir);
				return FAIL;		
			}
			
			closedir(dir);  
    } 
    else 
    {
        printf( "\n Error: directory option error.\n\n");
        usage();
        return FAIL;
    }
    
    
	 if (argv[3] != NULL && !strcmp(argv[3], "-sharedMemLoc")) 
    {
        if (argv[4] == NULL) 
        {
            printf("\n Error :Invalid shared memory location.\n\n");
            usage();
			return FAIL;
        }
        
        if( checkNum(argv[4] ) == FAIL)
		{
			printf("\n Please, Check shared memory location! \n Please, enter positive integer!");
			usage();
			return FAIL;
		}       
    } 
    else 
    {
        printf( "\n Error: sharedMemLoc option error.\n\n");
        usage();
        return FAIL;
    }      

	if (argv[5] != NULL && !strcmp(argv[5], "-startTime")) 
    {
        if (argv[6] == NULL) 
        {
            printf("\n Error: Invalid start time.\n\n");
            usage();
			return FAIL;
        }
        
        if( checkNum(argv[6] ) == FAIL)
		{
			printf("\n Please, Check start time! \n Please, enter positive integer!");
			usage();
			return FAIL;
		}       
    } 
    else 
    {
        printf( "\n Error: startTime option error.\n\n");
        usage();
        return FAIL;
    }
	

	if (argv[7] != NULL && !strcmp(argv[7], "-endTime")) 
    {
        if (argv[8] == NULL) 
        {
            printf("\n Error: Invalid end time.\n\n");
            usage();
			return FAIL;
        }
        
        if( checkNum(argv[8] ) == FAIL)
		{
			printf("\n Please, Check endtime! \n Please, enter positive integer!");
			usage();
			return FAIL;
		}       
    } 
    else 
    {
        printf( "\n Error: endTime option error.\n\n");
        usage();
        return FAIL;
    }
	
	
	return SUCCESS;
}


int checkNum(CHPTR argv)
{
	
	char temp[] = "0123456789";
	int i,k,flag=1;
	
	for(i=0; i< strlen(argv); ++i)
	{
		for(k=0; k<strlen(temp) && flag ; ++k)
		{
			if( argv[i] == temp[k])
				flag=0;			
		}
		
		if(flag)
			return FAIL;
		else
			flag=1;
		
	}
	
	
	return SUCCESS;
}




void usage(void)
{
	printf("\n ***************************************************************************\n");
	printf("\n Please, Enter 8 argumans!");
	printf("\n Usage: ");
	printf("\n      ./Generator -directory [#] -sharedMemLoc [#] -startTime [#]  -endTime  [#]   \n"
		
		   "\n      ./Generator     : execution name"
		   "\n      -directory      : option. Please write.."
		   "\n      [#]             : Enter Directory Name.."		   
		   "\n      -sharedMemLoc   : option. Please write.."
		   "\n      [#]             : Enter Shared Memory Location.."
		   "\n      -startTime      : option. Please write.."
		   "\n      [#]             : Enter integer number.( t ) to calculate function value "
		   "\n      -endTime        : option. Please write.."
		   "\n      [#]             : Enter integer number.( t ) to calculate function value"
		   "\n                           endTime must be greater than startTime.!!\n");
	
	printf("\n ***************************************************************************\n\n");	
}

