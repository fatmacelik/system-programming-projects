/*
 * 
 * GYTE 2014 BIL244 SYSTEM PROGRAMMING RETAKE
 * 
 * 
 * 20 HAZiRAN 2014 / CUMA / 23:50
 * 
 * FATMA CELiK  101044038
 * 
 * 
 * 
 * 
 * */

#include "generator.h"


CHPTRPTR files = NULL;
Function* funs = NULL;
Argumans arg; 
static int i=0;
struct timeval start,end;
pthread_t* tid=NULL;	
int freeNum=0;



/*
 * Program baslangici
 * Argumanlar kaydedilir
 * Generator cagrilir
 * Klasordeki dosyalar bulunur ve fonksiyonlar kaydedilir
 * 
 * 
 * */
int Start(CHPTRPTR argv)
{
	int num=0,i;

	strcpy(arg.dirName,argv[2]);
	strcpy(arg.location,argv[4]);
	arg.start = atoi(argv[6]);
	arg.end = atoi(argv[8]);	
	arg.numValue = (int) (arg.end - arg.start);
	
	findFilesNum(arg.dirName,&num);	
	arg.fileNum=num;
	freeNum = num;
	//printf("\n Files num = %d \n\n",num);
	
	files = (CHPTRPTR)malloc(num * sizeof(CHPTR));
	for(i=0; i<num; ++i)
		files[i] = (CHPTR)malloc(SIZE * sizeof(char));
	
	
	funs = (Function*)malloc(num * sizeof(Function));
	if(funs == NULL)
	{
		printf("\n Error malloc! ( function ) \n\n");
		free(funs);
		return FAIL;
	}
	
	for(i=0; i<num;++i)
	{
		funs[i].value = (DOUBLEPTR)malloc(arg.numValue * sizeof(double));
		funs[i].numValue = arg.numValue;
		funs[i].start = arg.start;
		funs[i].end = arg.end;
	}
	
	
	saveFilesName(arg.dirName,files,num);
	//printFilesName(files,num);				
	saveFunction(files,funs,num);

	Generator(funs,num, atoi(arg.location));
	
	Free(files,num);
	
	return SUCCESS;
}



/*
 * Generator modulu, parse, noiseAdder burda cagrilir
 * 
 * */
void Generator(Function* funs,int num, int shareLocation )
{
	int i;
	int funNum=0,opNum;
	//int status;
	
	printf("\n ***** Parsing Functions   *****\n");
	tid = (pthread_t*)malloc(num*sizeof(pthread_t));
	for(i=0; i<num ;++i)
	{		
		
		opNum = findOperatorNum(funs[i].f);	
		funs[i].numOfOperator=opNum;
	
		funs[i].op = (OPERATIONSPTR)malloc((opNum+1) * sizeof(OPERATIONS));
			
		funs[i].id = i+1;	
		pthread_create(&tid[i], NULL, &StartParse, &funs[i]);			
		pthread_join(tid[i],NULL);
										
		if(funs[i].id != -1)
		{
			funNum++;	
			printf("\n f%d ", funs[i].id);	
		}

	}
	
	printf("\n\n Function Parse End!\n");	
	printf("\n ***** Sending Functions to Shared Memory Block  *****\n");
	noiseAdder(funs,num,funNum, shareLocation);
	printf("\n NoiseAdder send functions to SharedMemory!\n Good Bye.\n\n");

	free(tid);
	FreeFuns(funs,num);
}




/*
 * Klasordeki dosya isimleri bulunur
 * */
void saveFilesName(CHPTR dirName, CHPTRPTR files,int fileNum)
{
	DIRPTR dirPtr; 			/* directory object from opendir() 			*/
    struct dirent *direntp; /* information about next directory entry   */
    char path[SIZE]; /* temporary char arrya for directory name  */ 	
		
	/* open the directroy */   
	dirPtr = opendir(dirName);

	/* opendir() status checked */
	if(dirPtr != NULL)
	{    		
		while( (direntp = readdir(dirPtr)) != NULL)
		{
			pathName(path,dirName,direntp->d_name);/*seth directory path name*/
            
			if( checkDirName(direntp->d_name) )
			{					
				if(!isdir(direntp) )
				{
					if( CheckPath(path)  == SUCCESS)
					{	
						if(i < fileNum)
						{						
							strcpy(files[i], path);
							i++;
						}
					}
				}
				else{						
					saveFilesName(path,files,fileNum);					
				}					
			}			
				
		}/*while end */
		
       closeDir(dirPtr); /* close directory */
	}/* if end */
}






/*
 * Fonksiyon bilgilerini shared Memory bloguna yazar
 * 
 * */
int noiseAdder(Function* funs, int num, int funNum, int shareLocation)
{
	int i,k;
    int shmid;
    key_t key;
    double *shm, *s;
	srand(time(NULL));

    // shared memory segment
    key = shareLocation;


	//create segment
    if ((shmid = shmget(key, SHMSZ, IPC_CREAT | 0666)) < 0) 
    {
        printf("\n shmget error \n");
        return FAIL;
    }

		
	// attach the segment to our data space.
	if ((shm = shmat(shmid, NULL, 0)) == (double *) -1) 
	{
		printf("\n shmat error \n");
		return FAIL;
	}
	
	// put something to read
	s = shm;	
	*s++ = funNum;
	*s++ = funs[0].numValue;

	for(i=0; i<num; ++i)
	{	
		if(funs[i].id  != -1)
		{
			*s++ = funs[i].id;
			*s++ = funs[i].start;
			*s++ = funs[i].end;
			
			for(k=0; k<funs[i].numValue; ++k)
			{
				funs[i].value[k] = funs[i].value[k] + rand()%1000;		
				*s++ = funs[i].value[k];					
			}		
		}
	}				
	
	*s = '\0';

	//wait until other process changes first charceter of memory
	while (*shm != '*')
		sleep(1);

	return SUCCESS;
	
}


/*
 * free to malloc
 * */
void FreeFuns(Function* funs, int num)
{
	int i;
	
	for(i=0; i<num; ++i)
	{
		if(funs[i].op != NULL)
			free(funs[i].op);

		if(funs[i].value != NULL)
			free(funs[i].value);

	}
	
	if(funs != NULL)
		free(funs);
	
}



/* Signal Handler for specific signals
   Gets two parameters which are handler func and signal flag
   Return -1 on error , otherwise returns true */
int SetHandler( Handler hand , int sig )
{
    if( signal(sig, hand) == SIG_ERR )
        return FAIL;

    return SUCCESS;
}


/*
 * control c handle edilir
 * free 'ler yapilir
 * ekrana mesaj yazdirilip cikilir
 * 
 * */
void InterruptHandlerParent(int sigNo)
{
    char msg[] = "\n ********************\n Generator died:( "
                 "\n ********************\n\n\n";	
	
    write(STDOUT_FILENO , msg , sizeof(msg));  
    
  	Free(files,freeNum);
  	
  	if(tid != NULL)
		free(tid);
		
	FreeFuns(funs,freeNum);

    exit(FAIL);      
}


