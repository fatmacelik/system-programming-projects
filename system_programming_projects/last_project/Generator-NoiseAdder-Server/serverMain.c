/*
 * 
 * GYTE 2014 BIL244 SYSTEM PROGRAMMING RETAKE
 * 
 * 
 * 20 HAZiRAN 2014 / CUMA / 23:50
 * 
 * FATMA CELiK  101044038
 * 
 * 
 * 
 * 
 * */



#include "server.h"

int main(int argc, CHPTRPTR argv)
{
	
	/* Handler ctrl + c */
    if( SetHandler( InterruptHandlerParent , SIGINT) == FAIL)
    {
        fprintf(stdout,"\n Failed to set signal handlers.\n\n");
        return FAIL;
    }
    
    /*-----------------------------------------------------------------*/
	
	if (checkArguman(argc,argv) == SUCCESS)
	{
		return Server(argv);
	}
	
	return 0;
}


/*
 * argumanlar kontrol edilir
 * */
int checkArguman(int argc, CHPTRPTR argv)
{
	    CLEAR_SCREEN;

    if (argc != 7) {
        usage();
        return FAIL;
    }

    if (strcmp(argv[0], "./Server") != 0) {
        printf("\n Please, Check execution name!");
        usage();
        return FAIL;
    }



    if (argv[1] != NULL && !strcmp(argv[1], "-sharedMemLoc")) {
        if (argv[2] == NULL) {
            printf("\n Error :Invalid shared memory location.\n\n");
            usage();
            return FAIL;
        }

        if (isNum(argv[2]) == FAIL) {
            printf("\n Please, Check shared memory location! \n Please, enter positive integer!");
            usage();
            return FAIL;
        }
    }
    else {
        printf("\n Error: sharedMemLoc option error.\n\n");
        usage();
        return FAIL;
    }

    if (argv[3] != NULL && !strcmp(argv[3], "-maxClient")) {
        if (argv[4] == NULL) {
            printf("\n Error :Invalid max client option.\n\n");
            usage();
            return FAIL;
        }

        if (isNum(argv[4]) == FAIL) {
            printf("\n Please, Check max client! \n Please, enter positive integer!");
            usage();
            return FAIL;
        }
    }
    else {
        printf("\n Error: max client option error.\n\n");
        usage();
        return FAIL;
    }

 
    if (argv[5] != NULL && !strcmp(argv[5], "-port")) {
        if (argv[6] == NULL) {
            printf("\n Error :Invalid port.\n\n");
            usage();
            return FAIL;
        }

        if (isNum(argv[6]) == FAIL) {
            printf("\n Please, Check port! \n Please, enter positive integer!");
            usage();
            return FAIL;
        }
    }
    else {
        printf("\n Error: port option error.\n\n");
        usage();
        return FAIL;
    }
	
	return SUCCESS;
}


int isNum(CHPTR argv)
{
	
	char temp[] = "0123456789";
	int i,k,flag=1;
	
	for(i=0; i< strlen(argv); ++i)
	{
		for(k=0; k<strlen(temp) && flag ; ++k)
		{
			if( argv[i] == temp[k])
				flag=0;			
		}
		
		if(flag)
			return FAIL;
		else
			flag=1;
		
	}
	
	
	return SUCCESS;
}



void usage(void)
{
	printf("\n ***************************************************************************\n");
    printf("\n Please, Enter 6 argumans!");
    printf("\n Usage: ");
    printf("\n      ./Server  -sharedMemLoc  [#]  -maxClient  [#]  -port  [#] \n"
    
		   "\n      ./Server        : execution name"
		   "\n      -sharedMemLoc   : option. Please write.."
		   "\n      [#]             : Enter Shared Memory Location.."
		   "\n      -maxClient      : option. Please write.."
		   "\n      [#]             : Enter number of max client."
		   "\n      -port           : option. Please write.."
		   "\n      [#]             : Enter port number.\n");
    printf("\n ***************************************************************************\n\n");
}

		  
