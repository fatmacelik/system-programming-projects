/*
 * 
 * GYTE 2014 BIL244 SYSTEM PROGRAMMING RETAKE
 * 
 * 
 * 20 HAZiRAN 2014 / CUMA / 23:50
 * 
 * FATMA CELiK  101044038
 * 
 * 
 * 
 * 
 * */

/*###########################   INCLUDES  ############################*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#include <sys/socket.h>
#include <errno.h>
#include <signal.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <pthread.h>

/*###########################   DEFINES  ############################*/

#define FALSE 0
#define TRUE 1

#define MAXHOSTNAME 30
#define MAXCONNECTION 10

#define SUCCESS 1
#define FAIL -1
#define CLEAR_SCREEN system("clear")

#define SHMSZ 27

#define MEMORY_FILE "memoryFile.txt"

#define SIZE 500000

/*###########################   TYPEDEFS  ############################*/


typedef int*            INTPTR;
typedef char**          CHPTRPTR;
typedef char*           CHPTR;
typedef double*         DOUBLEPTR;
typedef double**        DOUBLEPTRPTR;
typedef void*           VOIDPTR;
typedef pthread_t*      THREADPTR;
typedef void           (*Handler)(int);


typedef struct 
{
	int location;
	int maxClient;
	int port;

}Argumans;


typedef struct
{
	int id;
	int start;
	int end;
	
	int numValue;
	DOUBLEPTR value;
	
}Function;

/*####################### FUNCTION PROTOTYPES  ######################*/

void FreeFuns(Function* funs, int funNum);

int Server(CHPTRPTR argv);

void usage(void);

int checkArguman(int argc, CHPTRPTR argv);

int saveMemory(FILE* outp, int location);

int isNum(CHPTR argv);

int findFunNum(int* numValue);

double save(CHPTR temp);

void saveFunValues(Function* funs, int funNum);

void testPrintFun(Function* funs, int funNum);


/** SOCKET */
void StartServer(int portBegin, const int maxClient, int sharedMemory);

int SendMessage(int socket, VOIDPTR message, int size);

int CreateSocket(unsigned short portNum, int maxConnection);

int GetConnection(int s);

VOIDPTR ClientConnectionHandler(VOIDPTR arg);

VOIDPTR ClientDataHandler(VOIDPTR arg);

int SetHandler( Handler hand , int sig );

void InterruptHandlerParent(int sigNo);


