/*
 * 
 * GYTE 2014 BIL244 SYSTEM PROGRAMMING RETAKE
 * 
 * 
 * 20 HAZiRAN 2014 / CUMA / 23:50
 * 
 * FATMA CELiK  101044038
 * 
 * 
 * */


/*###########################   INCLUDES  ############################*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <ctype.h>
#include <pthread.h>
#include <math.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <signal.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h> 
#include <time.h> 
/*###########################   DEFINES  ############################*/

#define SUCCESS 1
#define FAIL -1
#define CLEAR_SCREEN system("clear")

#define SHMSZ 27
#define MS 1000

#define SIZE 500000

/*###########################   TYPEDEFS  ############################*/




/* fonksiyon katsayilarinin tutuldugu struct yapisi*/
typedef struct Operations
{
	char name;   		/* sin: s,  cos: c,  t: t        */
	int coef; 		/* coefficient of t              */
	int exponent; 	/* exponent of base. (t)         */
	
}operations_t;

 
typedef char**          CHPTRPTR;
typedef char*           CHPTR;
typedef double*         DOUBLEPTR;
typedef double**        DOUBLEPTRPTR;
typedef DIR*            DIRPTR; 
typedef operations_t* 	OPERATIONSPTR;
typedef operations_t 	OPERATIONS;
typedef void (*Handler)(int);/* Signal handler */


typedef struct 
{
	char dirName[SIZE];
	char location[50];
	int  fileNum;
	int start;
	int end;
	int numValue;
}Argumans;


typedef struct
{
	char f[SIZE];
	int id;
	
	int start;
	int end;
	
	int numValue;
	DOUBLEPTR value;
	
	OPERATIONSPTR op;
	int numOfOperator;
	
}Function; 



/*####################### FUNCTION PROTOTYPES  ######################*/


/************************** generator.h  *****************************/

int checkArguman(int argc, CHPTRPTR argv);

void usage(void);

int Start(CHPTRPTR argv);

void FreeFuns(Function* funs, int num);

void Generator(Function* funs,int num, int shareLocation );

int checkNum(CHPTR argv);

void saveFilesName(CHPTR dirName, CHPTRPTR files,int fileNum);

int SetHandler( Handler hand , int sig );

void InterruptHandlerParent(int sigNo);


/************************** noiseAdder.h  ****************************/

int noiseAdder(Function* funs, int num, int funNum, int shareLocation);

/************************** parser.h  ********************************/

void * StartParse(void *param);

int parse(CHPTR fun, OPERATIONSPTR op, int opNum);

int findOperatorNum(CHPTR fun);

int doParse(CHPTR fun, int i, OPERATIONS op, int index);

int isNum(char ch);

double calculateValue(OPERATIONSPTR op, double time, int num);

void printTestParse(OPERATIONSPTR operation, int numOfOperator);

int toDigit(char ch);

/************************  directory.h  ******************************/

/* check is directory */
int isdir( struct dirent *direntp );

/* close directory*/
void closeDir(DIRPTR dirPtr);

/* set directory path name */
void pathName(CHPTR path ,CHPTR dirName,CHPTR name );	

/*check path name*/
int CheckPath(CHPTR path);

void findFilesNum(CHPTR dirName, int *fileNum);

int checkDirName(CHPTR name);

void printFilesName(CHPTRPTR files,int num);

void Free(CHPTRPTR str ,int size);

void saveFunction(CHPTRPTR files, Function* funs, int num);

void printFunction(Function* funs, int num);

void checkFun(CHPTR fun);



