/*
 * 
 * GYTE 2014 BIL244 SYSTEM PROGRAMMING RETAKE
 * 
 * 
 * 20 HAZiRAN 2014 / CUMA / 23:50
 * 
 * FATMA CELiK  101044038
 * 
 * 
 * 
 * 
 * */
#include "server.h"


int sharedMemoryGlobal;
int funNum=0;
int max;
Function* funs;
int x=0;
INTPTR socketFree;
/*
 * Program baslangic fonksiyonu
 * */
int Server(CHPTRPTR argv)
{
	Argumans arg;
	FILE* outp;
	int numValue;
	int i;
	
	//argumanlar kaydedilir
	arg.location = atoi(argv[2]);
	arg.maxClient = atoi(argv[4]);
	arg.port = atoi(argv[6]);
	
	outp = fopen(MEMORY_FILE,"w");
	
	
	//shared memory den veriler alinir
	if( saveMemory(outp, arg.location) == FAIL)
	{
		fclose(outp);
		return FAIL;
	}
	
	fclose(outp);
	funNum = findFunNum(&numValue); 
	
	//printf("\n %d \n",funNum);
	funs = (Function*)malloc(funNum * sizeof(Function));
	if(funs == NULL)
	{
		printf("\n Error malloc! ( function ) \n\n");
		free(funs);
		return FAIL;
	}
	
	for(i=0; i<funNum ; ++i)
	{
		funs[i].value = (DOUBLEPTR)malloc(numValue * sizeof(double));
		funs[i].numValue = numValue;
	}
	saveFunValues(funs,funNum);	
	testPrintFun(funs,funNum);
	
	max=arg.maxClient;
	
	StartServer(arg.port, arg.maxClient, arg.location);

	FreeFuns(funs,funNum);
	
	return SUCCESS;
}


/*
 * free to malloc
 * */
void FreeFuns(Function* funs, int funNum)
{
	int i;
	
	for(i=0; i<funNum ; ++i)
	{
		if(funs[i].value != NULL)
			free(funs[i].value);		
	}
	
	free(funs);	
}

void testPrintFun(Function* funs, int funNum)
{
	int i;
	
	printf("\n ***********  SHARED MEMORY DATAS  ***********\n");
	for(i=0; i<funNum ; ++i)
	{
		printf("\n F%d  start: %d  end: %d   Value: %.3f ",funs[i].id, 
		                   funs[i].start,funs[i].end, funs[i].value[0]);		
	}
	
	putchar('\n');
}


/*
 * fonskiyon degerleri struct a kaydedilir
 * */
void saveFunValues(Function* funs, int funNum)
{
	int i,k;
	FILE* inp;
	char temp[SIZE];
	
	
	inp = fopen(MEMORY_FILE,"r");
	
	//empty read
	fgets(temp,SIZE,inp);
	fgets(temp,SIZE,inp);

	for(i=0; i<funNum;++i)
	{	
		fgets(temp,SIZE,inp);
		funs[i].id = (int)save(temp); 
		
		fgets(temp,SIZE,inp);
		funs[i].start= (int)save(temp); 
		
		fgets(temp,SIZE,inp);
		funs[i].end= (int)save(temp); 
		
		for(k=0; k<funs[i].numValue ; ++k)
		{	
			fgets(temp,SIZE,inp);
			funs[i].value[k] = save(temp); 
		}
	}

	fclose(inp);
}


double save(CHPTR temp)
{
	int i,flag=1;
	
	for(i=0; i<strlen(temp) && flag ; ++i)
	{
		if(temp[i] == ',')
		{
			temp[i] = '\0';
			flag=0;
		}
	}	
	
	return atof(temp);
	
}

/*
 * fonksiyon sayisi bulunur
 * */
int findFunNum(int* numValue)
{
	int num=0;
	FILE* inp;
	char temp[50];
	int i,flag=1;
	
	inp = fopen(MEMORY_FILE,"r");
	fgets(temp,50,inp);
	
	for(i=0; i<strlen(temp) && flag ; ++i)
	{
		if(temp[i] == ',')
		{
			temp[i] = '\0';
			flag=0;
		}
	}
	num = atoi(temp);

	fgets(temp,50,inp);
	flag=1;
	for(i=0; i<strlen(temp) && flag ; ++i)
	{
		if(temp[i] == ',')
		{
			temp[i] = '\0';
			flag=0;
		}
	}	
	*numValue = atoi(temp);	

	fclose(inp);
	return num;
}


/*
 * shared memeory deki bilgiler kaydedilir
 * */
int saveMemory(FILE* outp, int location)
{
	int shmid;
    key_t key;
    double *shm, *s;

	//segment
    key = location;

    // Locate the segment.
    if ((shmid = shmget(key, SHMSZ, 0666)) < 0) 
    {
        printf("\n shmget error \n");
        return FAIL;
    }		

	//attach the segment to our data space.
	if ((shm = shmat(shmid, NULL, 0)) == (double *) -1) 
	{
		printf("\n shmat error \n");
		return FAIL;
	}

	//read memory and put file
	for (s = shm; *s != '\0'; s++)
		fprintf(outp,"%.3f,\n",*s);

	//final chracter
	*shm = '*';
	
	return SUCCESS;
}



/**** SOCKET FUNCTIONS ****/
int CreateSocket(unsigned short portNum, int maxConnection) 
{
    char myname[MAXHOSTNAME + 1];
    int socketFd;
    struct sockaddr_in sa;
    struct hostent *hp;

    memset(&sa, 0, sizeof (struct sockaddr_in));

    gethostname(myname, MAXHOSTNAME);

    hp = gethostbyname(myname);

    if (hp == NULL) {
        return (FAIL);
    }

    sa.sin_family = hp->h_addrtype;
    sa.sin_port = htons(portNum);

    if ((socketFd = socket(AF_INET, SOCK_STREAM, 0)) == FAIL) 
    {
        printf("\n Failed to create socket\n");
        return (FAIL);
    }


    if (bind(socketFd, (struct sockaddr *) &sa, sizeof (struct sockaddr_in)) == FAIL) 
    {
        printf("\n Failed to bind\n");
        close(socketFd);
        return (FAIL);
    }

    listen(socketFd, maxConnection);

    return (socketFd);
}

/*
 * Soket baglantisi saglanir
 * **/
int GetConnection(int socketFd) {
    int retVal;

    if ((retVal = accept(socketFd, NULL, NULL)) == FAIL) 
    {
       // printf("Failed to accept");
        return FAIL;
    }

    return retVal;
}

/*
 * Client'a fonskiyon bilgiler gonderilir
 * 
 * */
int SendMessage(int socket, VOIDPTR message, int size) {
    int i,k,
        ret;
    Function* data = (Function*) message;

	if ((ret = write(socket, &x, sizeof(int))) == FAIL) 
	{
		printf("\n Failed to send data to socket\n");
		return FAIL;
	}
	
	if ((ret = write(socket, &size, sizeof(int))) == FAIL) 
	{
		printf("\n Failed to send data to socket\n");
		return FAIL;
	}
	if ((ret = write(socket, &data[0].numValue, sizeof(int))) == FAIL) 
	{
		printf("\n Failed to send data to socket\n");
		return FAIL;
	}


    for (i = 0; i < size; ++i) 
    {
        if ((ret = write(socket, &data[i].id, sizeof(int))) == FAIL) 
        {
            printf("\n Failed to send data to socket\n");
            return FAIL;
        }
        
        if ((ret = write(socket, &data[i].start, sizeof(int))) == FAIL) 
        {
            printf("\n Failed to send data to socket\n");
            return FAIL;
        }
        if ((ret = write(socket, &data[i].end, sizeof(int))) == FAIL) 
        {
            printf("\n Failed to send data to socket\n");
            return FAIL;
        }
        
        for(k=0; k< data[i].numValue; ++k)
        {
			if ((ret = write(socket, &data[i].value[k], sizeof(double))) == FAIL) 
			{
				printf("\n Failed to send data to socket\n");
				return FAIL;
			}
			
		}
        
    }

    return SUCCESS;
}


/*
 * 
 * Client'lar ile connection saglanir'
 * 
 * */
void StartServer(int portBegin, const int maxClient, int sharedMemory)
{
    int i;
    int sockets[maxClient];
    pthread_t handlers[maxClient];

	socketFree = sockets;

    // Shared memory'den verileri almak ve clientlere gondermek icin ClientDataHandler'a lazim
    sharedMemoryGlobal = sharedMemory;

    // Her port icin soket ac ve soketleri kaydet
    for (i = 0; i < maxClient; ++i) 
    {
        sockets[i] = CreateSocket(portBegin + i, MAXCONNECTION);
        // Her thread bir portu dinlicek, acilan soket thread'lere veriliyor
        if (pthread_create(&handlers[i], NULL, ClientConnectionHandler, &sockets[i])) 
        {
            printf( "\nError: Failed to create connection handler\n");
            return;
        }
    }

    // thread'leri bekler
    for (i = 0; i < maxClient; ++i) {
        pthread_join(handlers[i], NULL);
    }

}


/*
 * Client'a veriler, soket ile gonderilir
 * 
 * */
VOIDPTR ClientDataHandler(VOIDPTR arg) 
{
    INTPTR socketFd = (INTPTR) arg;    
    
    SendMessage(*socketFd, funs, funNum);
    close(*socketFd);
    // oldur kendini, cunku bu threadi bekleyen yok
    pthread_detach(pthread_self());
    pthread_exit(NULL);

}

/*
 * Client baglantilarini bekler
 * */
VOIDPTR ClientConnectionHandler(VOIDPTR arg)
{

    int socketFd, i;
    INTPTR socket = (INTPTR) arg;
    pthread_t dataHandler;


    /* The loop which handles incoming connections */
    i = 0;
    for (;;) {
        if ((socketFd = GetConnection(*socket)) == FAIL) 
        {
           // printf( "\n Error:Failed to accept connection.\n");
        }
        // Bu porta birden fazla client baglanabilir, her biri icin bir thread
        // Gelen baglantinin fd'si thread'e veriliyor haberlesme icin
       else
       { 
		    printf("\n Server: Data sent to Client. \n");
			x++;

			if (pthread_create(&dataHandler, NULL, ClientDataHandler, &socketFd) == FAIL) 
		    {
				printf( "\nError: Failed to create data handler\n");
		    }

		}
        i = (i + 1) % MAXCONNECTION;
    }/* End of for */

    close(*socket);
    pthread_exit(NULL);
}



/* Signal Handler for specific signals
   Gets two parameters which are handler func and signal flag
   Return -1 on error , otherwise returns true */
int SetHandler( Handler hand , int sig )
{
    if( signal(sig, hand) == SIG_ERR )
        return FAIL;

    return SUCCESS;
}


/*
 * control c handle edilir
 * free 'ler yapilir
 * ekrana mesaj yazdirilip cikilir
 * 
 * */
void InterruptHandlerParent(int sigNo)
{
    char msg[] = "\n ********************"
                 "\n Server died:( "
                 "\n ********************\n\n\n";	
	
    write(STDOUT_FILENO , msg , sizeof(msg));     
	FreeFuns(funs,funNum);
  	
  	close(*socketFree);
    exit(FAIL);      
}


