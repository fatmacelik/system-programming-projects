/*
 * 
 * GYTE 2014 BIL244 SYSTEM PROGRAMMING RETAKE
 * 
 * 
 * 20 HAZiRAN 2014 / CUMA / 23:50
 * 
 * FATMA CELiK  101044038
 * 
 * 
 * 
 * 
 * */

#include "generator.h"


/*
 * Fonksiyon degerleri struct a kaydedilir
 * */
void saveFunction(CHPTRPTR files, Function* funs, int num)
{
	int i;
	FILE* inp;
	
	for(i=0; i<num; ++i)
	{	
		inp = fopen(files[i],"r");		
		fgets(funs[i].f, SIZE,inp);
		checkFun(funs[i].f);		
		fclose(inp);
	}
	
	//printFunction(funs,num);
}

/*
 * fonksiyon formati kontrol edilir
 * 
 * */
void checkFun(CHPTR fun)
{
	int i,count=0,flag=1;
	
	for(i=0; i<strlen(fun) && flag; ++i)
	{
		if(fun[i]== '"')
			count++;
			
		if(count == 2)
		{
			fun[i+1]='\0';
			flag=0;
			
		}	
	}

}

void printFunction(Function* funs, int num)
{
	int i;
	
	for(i=0; i<num; ++i)
	{
		printf("\n %s",funs[i].f);
	}	
	printf("\n\n");
	
}

void printFilesName(CHPTRPTR files,int num)
{
	int i;
	
	for(i=0; i<num; ++i)
		printf("\n %s",files[i]);
		
	printf("\n\n");
}

/*
 * Klasordeki dosya sayisini bulur
 * 
 * */
void findFilesNum(CHPTR dirName, int* fileNum)
{
	DIRPTR dirPtr; 			/* directory object from opendir() 			*/
    struct dirent *direntp; /* information about next directory entry   */
    char path[SIZE]; /* temporary char arrya for directory name  */ 	
		
	/* open the directroy */   
	dirPtr = opendir(dirName);

	/* opendir() status checked */
	if(dirPtr != NULL)
	{    		
		while( (direntp = readdir(dirPtr)) != NULL)
		{
			pathName(path,dirName,direntp->d_name);/*seth directory path name*/
            
			if( checkDirName(direntp->d_name) )
			{					
				if(!isdir(direntp) )
				{
					if( CheckPath(path)  == SUCCESS)
					{	
						(*fileNum)++;
					}
				}
				else{						
					findFilesNum(path,fileNum);					
				}					
			}			
				
		}/*while end */
		
       closeDir(dirPtr); /* close directory */
	}/* if end */
		
}


// directory islemleri icin yardimci fonksiyonlar
void closeDir(DIRPTR dirPtr)
{
    while ((closedir(dirPtr) == -1) && (errno == EINTR)) ;
}

void pathName(CHPTR path ,CHPTR dirName,CHPTR name)
{
	strcpy(path,dirName);
	strcat(path,"/");
	strcat(path,name);   
}

int isdir( struct dirent *direntp )
{
    return direntp->d_type == DT_DIR ;          
}

int checkDirName(CHPTR name)
{
    return strcmp("." , name) && strcmp("..", name);
}

int CheckPath(CHPTR path)
{
	int i=0;

	for(i=0; i< strlen(path) ; ++i)
		if(path[i] == '~')
			return FAIL;
			
	return SUCCESS;
}

// free to malloc
void Free(CHPTRPTR str ,int size)
{
	int i;

	for(i=0; i< size; i++)
	{
		if(str[i] != NULL)
			free(str[i]);
	}

	if(str != NULL)
		free(str);
}
