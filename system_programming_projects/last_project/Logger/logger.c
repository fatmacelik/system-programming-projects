/*
 * 
 * GYTE 2014 BIL244 SYSTEM PROGRAMMING RETAKE
 * 
 * 
 * 20 HAZiRAN 2014 / CUMA / 23:50
 * 
 * FATMA CELiK  101044038
 * 
 * 
 * */
 
 
#include "logger.h"

INTPTR socketFree;
sem_t clientSem;    

/*
 * Kullanicidan max worker thread ve port numaralarini alir
 * semafor olusturulur
 * Soketler olusturulur
 * Client baglantilari beklenir
 * 
 * */

void StartLogger(const int maxWorker, int port)
{
    int socket;
    int socketFd;
    pthread_t worker;

    sem_init(&clientSem, 0, maxWorker);

    socket = CreateSocket(port, MAXCONNECTION);
	socketFree = &socket;
    for (;;) {
        if ((socketFd = GetConnection(socket)) == FAIL) 
        {
            printf( "\n Error:Failed to accept connection.\n");
            break;
        }

        if (pthread_create(&worker, NULL, ClientHandler, &socketFd)) 
        {
            printf("\nError: Failed to create client connection handler\n");
            break;
        }
    }

    sem_destroy(&clientSem);
}


/*
 * Verilen port numarasina gore, soket olusturulur, maxConnection
 * kadar soket olusturulur
 * */
int CreateSocket(unsigned short portNum, int maxConnection)
{
    char myname[MAXHOSTNAME + 1];
    int socketFd;
    struct sockaddr_in sa;
    struct hostent *hp;

    memset(&sa, 0, sizeof (struct sockaddr_in));

    gethostname(myname, MAXHOSTNAME);

    hp = gethostbyname(myname);

    if (hp == NULL) {
        return (FAIL);
    }

    sa.sin_family = hp->h_addrtype;
    sa.sin_port = htons(portNum);

    if ((socketFd = socket(AF_INET, SOCK_STREAM, 0)) == FAIL) 
    {
        printf("\n Failed to create socket\n");
        return (FAIL);
    }


    if (bind(socketFd, (struct sockaddr *) &sa, sizeof (struct sockaddr_in)) == FAIL) 
    {
        printf("\n Failed to bind\n");
        close(socketFd);
        return (FAIL);
    }

    listen(socketFd, maxConnection);

    return (socketFd);
}


/*
 * Soket baglantisini kontrol eder
 * */
int GetConnection(int socketFd)
{
    int retVal;

    if ((retVal = accept(socketFd, NULL, NULL)) == FAIL) 
    {
        printf("\n Failed to accept\n");
        return FAIL;
    }

    return retVal;
}


/*
 * Client dan fonskiyon bilgilerini alir
 * */
int ReceiveMessage(int socket, VOIDPTR message, int size)
{
    int i,k,
        ret;
	int funNum;
	int temp;
	double d;
	int val;
	int numValue;
	int clientNum;
	char file[] = "client0#.log";
	FILE* outp;

	if ((ret = read(socket, &clientNum, sizeof(int))) == FAIL) 
	{
		printf("\n Failed to send data to socket\n");
		return FAIL;
	}
    
    fprintf(stdout,"\n Client Data Received.\n");

	if(clientNum < 10)	
		file[7] = clientNum + '0';
	else if(clientNum > 9)
	{
		file[6] = (clientNum/10) + '0';
		file[7] = (clientNum%10) + '0';	
	}		
	outp = fopen(file,"w");
	 
	 
	if ((ret = read(socket, &funNum, sizeof(int))) == FAIL) 
	{
		printf("\n Failed to read data to socket\n");
		return FAIL;
	}
	
	if ((ret = read(socket, &numValue, sizeof(int))) == FAIL) 
	{
		printf("\n Failed to read data to socket\n");
		return FAIL;
	}


    for (i = 0; i < funNum; ++i) 
    {
       if ((ret = read(socket, &temp, sizeof(int))) == FAIL) //id
        {
            printf("\n Failed to send data to socket\n");
            return FAIL;
        }
        
        //fprintf(outp,"\n\nFUNCTION ID:  %d,\n",temp);
        
        if ((ret = read(socket, &temp, sizeof(int))) == FAIL) //start
        {
            printf("\n Failed to send data to socket\n");
            return FAIL;
        }
        
        //fprintf(outp,"START TIME:  %d,\n",temp);

        if ((ret = read(socket, &temp, sizeof(int))) == FAIL) //end
        {
            printf("\n Failed to send data to socket\n");
            return FAIL;
        }
        
        //fprintf(outp,"END TIME:   %d,\n",temp);

        fprintf(outp,"\n\nVALUES:  \n");

        for(k=0; k< numValue; ++k)
        {
			if ((ret = read(socket, &d, sizeof(double))) == FAIL) //value
			{
				printf("\n Failed to send data to socket\n");
				return FAIL;
			}
			
			val = (int)d;
			if( val != IGNORE )
				fprintf(outp,"%.3f,\n",d);

			
		}
    }
    fprintf(outp,"\n ################# END ##################\n");


	fclose(outp);
	
    return SUCCESS;
}


/*
 * Baglanan client dan veriler alinir
 * */
VOIDPTR ClientHandler(VOIDPTR arg)
{
    int socketFd = *((INTPTR) arg);
    char data[20];
    sem_wait(&clientSem);
    
    printf("\n Waiting Clients..\n");
    // Client'ten verileri al
    ReceiveMessage(socketFd, data, 20);

    close(socketFd);

    sem_post(&clientSem);
    pthread_exit(NULL);
}



/* Signal Handler for specific signals
   Gets two parameters which are handler func and signal flag
   Return -1 on error , otherwise returns true */
int SetHandler( Handler hand , int sig )
{
    if( signal(sig, hand) == SIG_ERR )
        return FAIL;

    return SUCCESS;
}


/*
 * control c handle edilir
 * free 'ler yapilir
 * ekrana mesaj yazdirilip cikilir
 * 
 * */
void InterruptHandlerParent(int sigNo)
{
    char msg[] = "\n ********************\n Logger died:( "
                 "\n ********************\n\n\n";	
	
    write(STDOUT_FILENO , msg , sizeof(msg));  
    
	close(*socketFree);

    exit(FAIL);      
}



