/*
 * 
 * GYTE 2014 BIL244 SYSTEM PROGRAMMING RETAKE
 * 
 * 
 * 20 HAZiRAN 2014 / CUMA / 23:50
 * 
 * FATMA CELiK  101044038
 * 
 * 
 * 
 * 
 * */
 
 
#include "logger.h"


int main(int argc, CHPTRPTR argv)
{	
	Argumans arg;
	
	/* Handler ctrl + c */
    if( SetHandler( InterruptHandlerParent , SIGINT) == FAIL)
    {
        fprintf(stdout,"\n Failed to set signal handlers.\n\n");
        return FAIL;
    }
    
    /*-----------------------------------------------------------------*/
	
	
	if (checkArguman(argc,argv,&arg.maxWorker, &arg.port) == SUCCESS)
	{
		
		printf("\n Waiting Clients:..\n");
		StartLogger(arg.maxWorker,arg.port);
	}
	
	return 0;
}


/*
 * Logger argumanlari kontrol edilir
 * */
int checkArguman(int argc, CHPTR argv[], INTPTR maxWorker, INTPTR port)
{
    CLEAR_SCREEN;
    
    if (argc != 5) 
    {
        printf( "\n Error :Missing argument.\n\n");
        usage();
        return FAIL;
    }

    if (argv[1] != NULL && !strcmp(argv[1], "-maxWorker")) 
    {
        if (argv[2] == NULL) 
        {
            printf( "\n Error :Invalid max worker.\n\n");
            usage();
            return FAIL;
        }
        
        if(isNum(argv[2]) )
			*maxWorker = atoi(argv[2]);
		else
		{
			printf( "\n Error :Invalid max worker value. Please numbers for maxworker.\n\n");
            usage();
            return FAIL;			
		}
    }
    else {
        printf( "\n Error :Shared memory option error.\n\n");
        usage();
        return FAIL;
    }

    if (argv[3] != NULL && !strcmp(argv[3], "-port")) 
    {
        if (argv[4] == NULL) 
        {
            printf( "\n Error :Invalid port.\n\n");
            usage();
            return FAIL;
        }            
          
        if(isNum(argv[4]) )
			*port = atoi(argv[4]);
		else
		{
			printf( "\n Error :Invalid port. Please numbers for port.\n\n");
            usage();
            return FAIL;			
		}

    }
    else
    {
        printf( "\n Error :Port option error.\n\n");
        usage();
        return FAIL;
    }
    
    return SUCCESS;

}


/*
 * argumanlarda rakamlardan olusup olusmadigini kontrol eder
 * */
int isNum(CHPTR argv)
{
	
	char temp[] = "0123456789";
	int i,k,flag=1;
	
	for(i=0; i< strlen(argv); ++i)
	{
		for(k=0; k<strlen(temp) && flag ; ++k)
		{
			if( argv[i] == temp[k])
				flag=0;			
		}
		
		if(flag)
			return FAIL;
		else
			flag=1;
		
	}
	
	
	return SUCCESS;
}



void usage(void)
{
	printf("\n ***************************************************************************\n");
	printf("\n Please, Enter 4 argumans!");
	printf("\n Usage: ");
	printf("\n      ./Logger -maxWorker [#] -port [#] \n");
	
	printf("\n      ./Logger     : execution name "
	       "\n      -maxWorker   : option. Please write.."
	       "\n      [#]          : Enter number of max worker threads."
	       "\n      -port        : option. Please write.."
		   "\n      [#]          : Enter port number.\n");

	printf("\n ***************************************************************************\n\n");	
}


