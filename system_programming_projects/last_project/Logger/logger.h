/*
 * 
 * GYTE 2014 BIL244 SYSTEM PROGRAMMING RETAKE
 * 
 * 
 * 20 HAZiRAN 2014 / CUMA / 23:50
 * 
 * FATMA CELiK  101044038
 * 
 * 
 * 
 * 
 * */
 
 
/*###########################   INCLUDES  ############################*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#include <sys/socket.h>
#include <errno.h>
#include <signal.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <pthread.h>
#include <semaphore.h>


/*###########################   DEFINES  ############################*/

#define FALSE 0
#define TRUE 1

#define MAXHOSTNAME 30
#define MAXCONNECTION 10

#define SUCCESS 1
#define FAIL -1
#define CLEAR_SCREEN system("clear")

#define SHMSZ 27

#define MEMORY_FILE "memoryFile.txt"

#define SIZE 500000

#define IGNORE  9999

/*###########################   TYPEDEFS  ############################*/

typedef struct 
{
	int maxWorker;
	int port;

}Argumans;


typedef struct
{
	int id;
	double time;
	double value;
	
}Function;


typedef int*            INTPTR;
typedef char**          CHPTRPTR;
typedef char*           CHPTR;
typedef double*         DOUBLEPTR;
typedef double**        DOUBLEPTRPTR;
typedef void*           VOIDPTR;
typedef pthread_t*      THREADPTR;
typedef void           (*Handler)(int);

/*####################### FUNCTION PROTOTYPES  ######################*/


void StartLogger(const int maxWorker,int port);

int ReceiveMessage(int socket, VOIDPTR message, int size);

int CreateSocket(unsigned short portNum, int maxConnection);

int GetConnection(int s);

VOIDPTR ClientHandler(VOIDPTR arg);

int checkArguman(int argc, CHPTR argv[], INTPTR maxWorker, INTPTR port);

void usage(void);

int isNum(CHPTR argv);

int SetHandler( Handler hand , int sig );

void InterruptHandlerParent(int sigNo);

