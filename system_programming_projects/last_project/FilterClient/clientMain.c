/*
 * 
 * GYTE 2014 BIL244 SYSTEM PROGRAMMING RETAKE
 * 
 * 
 * 20 HAZiRAN 2014 / CUMA / 23:50
 * 
 * FATMA CELiK  101044038
 * 
 * 
 * */
 
 
#include "filterClient.h"


int main(int argc, CHPTR argv[]) 
{
	Argumans arg;
	if (checkArguman(argc,argv, &arg.cutoff, &arg.damping, arg.server, 
	                 &arg.port,arg.logger, &arg.lport) == SUCCESS )
	{
		return StartClient(arg.cutoff, arg.damping, arg.server, arg.port, arg.logger,arg.lport);
	}
	
	return 0;
}

/*
 * Arguman kontrolu yapar
 * 
 * */
int checkArguman(int argc, CHPTR argv[], DOUBLEPTR cutoff, DOUBLEPTR damping, 
                 CHPTR server, INTPTR port, CHPTR logger, INTPTR loggerPort) 
{
	CLEAR_SCREEN;
	
    if (argc != 13) 
    {
        printf( "\nError :Missing argument.\n\n");
        usage();
        return FAIL;
    }


	if (strcmp(argv[0], "./FilterClient") != 0) 
	{
		printf("\n Please, Check execution name!");
		usage();
		return FAIL;
	}
    
    if (argv[1] != NULL && !strcmp(argv[1], "-cutoff")) 
    {
        if (argv[2] == NULL) 
        {
            printf("\n Error :Invalid cutoff.\n\n");
            usage();
			return FAIL;
        }
        sscanf(argv[2], "%lf", cutoff);
    } 
    else 
    {
        printf( "\n Error :Cutoff option error.\n\n");
        usage();
        return FAIL;
    }

    if (argv[3] != NULL && !strcmp(argv[3], "-damping")) 
    {
        if (argv[4] == NULL) 
        {
            printf( "\nError :Invalid damping.\n\n");
            usage();
            return FAIL;
        }
       sscanf(argv[4], "%lf", damping);
    } 
    else 
    {
        printf( "\n Error :Damping option error.\n\n");
        usage();
       return FAIL;
    }

    if (argv[5] != NULL && !strcmp(argv[5], "-server")) 
    {
        if (argv[6] == NULL) 
        {
            printf( "\nError :Invalid server.\n\n");
            usage();
            return FAIL;
        }
        strcpy(server, argv[6]);
    } 
    else 
    {
        printf("\n Error :Server option error.\n\n");
        usage();
        return FAIL;
    }

    if (argv[7] != NULL && !strcmp(argv[7], "-port")) 
    {
        if (argv[8] == NULL) {
            printf( "\n Error :Invalid port.\n\n");
            usage();
            return FAIL;
        }
        *port = atoi(argv[8]);
    }
    else 
    {
        printf( "\n Error :Port option error.\n\n");
        usage();
        return FAIL;
    }
    
    
     if (argv[9] != NULL && !strcmp(argv[9], "-logger")) 
     {
        if (argv[10] == NULL) 
        {
            fprintf(stderr, "\nError :Invalid logger address.\n\n");
            usage();
            return FAIL;
        }
        strcpy(logger, argv[10]);
    }
    else {
        fprintf(stderr, "\nError :Logger option error.\n\n");
        usage();
        return FAIL;
    }

    if (argv[11] != NULL && !strcmp(argv[11], "-loggerPort")) 
    {
        if (argv[12] == NULL) {
            fprintf(stderr, "\nError :Invalid logger port.\n\n");
            usage();
            return FAIL;
        }
        *loggerPort = atoi(argv[12]);
    }
    else {
        fprintf(stderr, "\nError :Logger Port option error.\nTry -h.\n\n");
        usage();
		return FAIL;
	}


	return SUCCESS;
}



void usage() 
{
	printf("\n *******************************************************************\n");
    printf("\n The Client program receiving function data from server.");
    printf("\n Please enter 12 argumans.");
    printf("\n Usage:"
		   "\n       ./FilterClient -cutoff [#] -damping [#] -server [#] -port [#] -logger [#] -loggerPort [#]\n"
		   
		   "\n       ./FilterClient  : execution name "
	       "\n       -cutoff         : option. Please write.."
	       "\n       [#]             : Enter cut of frequency."
	       "\n       -damping        : option. Please write.."
		   "\n       [#]             : Enter damping ratio."
		   "\n       -server         : option. Please write.."
		   "\n       [#]             : Enter Host Name.(localhost).."
		   "\n       -port           : option.Please write."
		   "\n       [#]             : Enter server port num."
		   "\n       -logger         : option.Please write."
		   "\n       [#]             : Enter Host Name.(localhost)..."
		   "\n       -loggerPort     : option.Please write."
		   "\n       [#]             : Enter logger port num.\n"
		   
		   "\n *******************************************************************\n\n");
}	
	
