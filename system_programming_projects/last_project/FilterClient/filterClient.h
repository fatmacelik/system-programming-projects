/*
 * 
 * GYTE 2014 BIL244 SYSTEM PROGRAMMING RETAKE
 * 
 * 
 * 20 HAZiRAN 2014 / CUMA / 23:50
 * 
 * FATMA CELiK  101044038
 * 
 * 
 * 
 * 
 * */

/*###########################   INCLUDES  ############################*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <float.h>
#include <math.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#include <sys/socket.h>
#include <errno.h>
#include <signal.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <pthread.h>

/*###########################   DEFINES  ############################*/

#define FALSE 0
#define TRUE 1

#define MAXHOSTNAME 30

#define SUCCESS 1
#define FAIL -1
#define CLEAR_SCREEN system("clear")

#define SHMSZ 27

#define MEMORY_FILE "memoryFile.txt"

#define SIZE 500000

#define BILLION 1000000000L
#define IGNORE  9999


/*###########################   TYPEDEFS  ############################*/


typedef int*            INTPTR;
typedef char**          CHPTRPTR;
typedef char*           CHPTR;
typedef double*         DOUBLEPTR;
typedef double**        DOUBLEPTRPTR;
typedef void*           VOIDPTR;
typedef pthread_t*      THREADPTR;
typedef void           (*Handler)(int);



typedef struct
{
	int id;
	int start;
	int end;
	
	int numValue;
	DOUBLEPTR value;
	
}Function;



typedef struct 
{
	double cutoff;
	double damping;
	int port;
	char server[MAXHOSTNAME];
	int lport;
	char logger[MAXHOSTNAME];

}Argumans;

/*####################### FUNCTION PROTOTYPES  ######################*/

int checkArguman(int argc, CHPTR argv[],DOUBLEPTR cutoff, DOUBLEPTR damping, 
                      CHPTR server, INTPTR port, 
                      CHPTR logger, INTPTR loggerPort); 

void usage(void);

void saveFuns(void);

double save(CHPTR temp);

int ReceiveMessage(int socket, VOIDPTR message, int size);

int SendMessage(int socket, VOIDPTR message, int size);

int StartClient(double cutoff, double damping, CHPTR server,
                          int port, CHPTR logger, int loggerPort);

int ConnectServer(CHPTR hostname, unsigned short portNum);

void filtrele(double  cutoff, double damping);

void FreeFuns(Function* funs, int funNum);



