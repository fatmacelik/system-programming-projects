/*
 * 
 * GYTE 2014 BIL244 SYSTEM PROGRAMMING RETAKE
 * 
 * 
 * 20 HAZiRAN 2014 / CUMA / 23:50
 * 
 * FATMA CELiK  101044038
 * 
 * 
 * 
 * 
 * */
#include "filterClient.h"







int funNum;
int numValue;
Function* funs;
int clientNum;
char file[] = "client0#.txt";

/* Starts client program */
int StartClient(double cutoff, double damping, CHPTR server, int port, CHPTR logger, int loggerPort)
{
    int serverSocketFd, loggerSocketFd;
    char data[20];
    int i;

    if ((serverSocketFd = ConnectServer(server, port)) == FAIL) 
    {
        printf( "\n Error : Failed to connect server.\n");
        return FAIL;
    }

    //Shared memory'den bilgiyi al
    ReceiveMessage(serverSocketFd, data, 20);

	funs = (Function*)malloc(funNum * sizeof(Function));
	for(i=0; i<funNum; ++i)
	{
		funs[i].value = (DOUBLEPTR)malloc(numValue * sizeof(double));
		funs[i].numValue = numValue;
	}
	
	saveFuns();
	filtrele(cutoff,damping);


	//logger ile baglanti kur
    if ((loggerSocketFd = ConnectServer(logger, loggerPort)) == FAIL) 
    {
        printf( "\n Error : Failed to connect logger.\n");
        close(serverSocketFd);
        return FAIL;
    }
    
    printf("\n Client Data Sent to Logger. \n ");  
    SendMessage(loggerSocketFd, funs, funNum);

    close(serverSocketFd);
    close(loggerSocketFd);
    
    FreeFuns(funs,funNum);
    
    return SUCCESS;
}

/*
 * free to malloc
 * */
void FreeFuns(Function* funs, int funNum)
{
	int i;
	
	for(i=0; i<funNum ; ++i)
	{
		if(funs[i].value != NULL)
			free(funs[i].value);		
	}
	
	free(funs);	
}

/*
 * filtreleme islemleri gercekelestirilir
 * */
void filtrele(double  cutoff, double damping)
{
	int i,k;
	
	for(i=0; i<funNum ; ++i)
	{
		for(k=0; k<numValue ; ++k)
		{
			if(funs[i].value[k] > cutoff)
				funs[i].value[k] = IGNORE;
			else
			{
				funs[i].value[k] = funs[i].value[k] * damping;				
			}			
		}	
		
	}

}

/*
 * text dosyasindaki degerleri, struct a kaydeder
 * */
void saveFuns(void)
{
	int i,k;
	FILE* inp;
	char temp[500];
	
	inp = fopen(file,"r");
	
	//empty read
	fgets(temp,SIZE,inp);
	fgets(temp,SIZE,inp);

	for(i=0; i<funNum;++i)
	{	
		fgets(temp,SIZE,inp);
		funs[i].id = (int)save(temp); 
		
		fgets(temp,SIZE,inp);
		funs[i].start= (int)save(temp); 
		
		fgets(temp,SIZE,inp);
		funs[i].end= (int)save(temp); 
		
		for(k=0; k<funs[i].numValue ; ++k)
		{	
			fgets(temp,SIZE,inp);
			funs[i].value[k] = save(temp); 
		}
	}

	fclose(inp);
	
}

/*
 * string deki ',' virgulleri ignore eder
 * */
double save(CHPTR temp)
{
	int i,flag=1;
	
	for(i=0; i<strlen(temp) && flag ; ++i)
	{
		if(temp[i] == ',')
		{
			temp[i] = '\0';
			flag=0;
		}
	}	
	
	return atof(temp);
	
}



/* Connects server using the key */
int ConnectServer(CHPTR hostname, unsigned short portNum)
{
    struct sockaddr_in sa;
    struct hostent *hp;
    int s;

    if ((hp = gethostbyname(hostname)) == NULL) 
    {
        errno = ECONNREFUSED;
        printf("\n Failed to get hostname \n");
        return FAIL;
    }

    memset(&sa, 0, sizeof (sa));
    memcpy((char *) &sa.sin_addr, hp->h_addr, hp->h_length);

    sa.sin_family = hp->h_addrtype;
    sa.sin_port = htons((u_short) portNum);

    if ((s = socket(hp->h_addrtype, SOCK_STREAM, 0)) == FAIL) 
    {
        printf("\n Failed to get socket\n");
        return FAIL;
    }

    if (connect(s, (struct sockaddr *) &sa, sizeof sa) == FAIL) 
    {
        printf("\n Failed connect socket \n");
        close(s);
        return FAIL;
    }

    return (s);
}

/*
 * 
 * Server dan, soket ile fonskiyon bilgilerini alir
 * 
 * */
int ReceiveMessage(int socket, VOIDPTR message, int size)
{
    //CHPTR data = (CHPTR) message;
    int i,k,
        ret;
    FILE* outp;    
    int temp;
	double d;

	if ((ret = read(socket, &clientNum, sizeof(int))) == FAIL) 
	{
		printf("\n Failed to send data to socket\n");
		return FAIL;
	}
    printf("\n Client Data Received from Server. \n ");

	if(clientNum < 10)	
		file[7] = clientNum + '0';
	else if(clientNum > 9)
	{
		file[6] = (clientNum/10) + '0';
		file[7] = (clientNum%10) + '0';	
	}		
	outp = fopen(file,"w");
	 
	if ((ret = read(socket, &funNum, sizeof(int))) == FAIL) 
	{
		printf("\n Failed to read data to socket\n");
		return FAIL;
	}
	
	if ((ret = read(socket, &numValue, sizeof(int))) == FAIL) 
	{
		printf("\n Failed to read data to socket\n");
		return FAIL;
	}


    for (i = 0; i < funNum; ++i) 
    {
       if ((ret = read(socket, &temp, sizeof(int))) == FAIL) //id
        {
            printf("\n Failed to send data to socket\n");
            return FAIL;
        }
        
        fprintf(outp,"%d,\n",temp);
        
        if ((ret = read(socket, &temp, sizeof(int))) == FAIL) //start
        {
            printf("\n Failed to send data to socket\n");
            return FAIL;
        }
        
        fprintf(outp,"%d,\n",temp);

        if ((ret = read(socket, &temp, sizeof(int))) == FAIL) //end
        {
            printf("\n Failed to send data to socket\n");
            return FAIL;
        }
        
        fprintf(outp,"%d,\n",temp);

        for(k=0; k< numValue; ++k)
        {
			if ((ret = read(socket, &d, sizeof(double))) == FAIL) //value
			{
				printf("\n Failed to send data to socket\n");
				return FAIL;
			}
			fprintf(outp,"%.3f,\n",d);

			
		}
    }

	fclose(outp);
	
    return SUCCESS;
}


/*
 * logger'a  soket ile filtrelenmis verileri gonderir
 * */
int SendMessage(int socket, VOIDPTR message, int size)
{
    int i,k,
        ret;
    Function* data = (Function*) message;

	if ((ret = write(socket, &clientNum, sizeof(int))) == FAIL) 
	{
		printf("\n Failed to send data to socket\n");
		return FAIL;
	}
	
	if ((ret = write(socket, &size, sizeof(int))) == FAIL) 
	{
		printf("\n Failed to send data to socket\n");
		return FAIL;
	}
	
	if ((ret = write(socket, &data[0].numValue, sizeof(int))) == FAIL) 
	{
		printf("\n Failed to send data to socket\n");
		return FAIL;
	}


    for (i = 0; i < size; ++i) 
    {
        if ((ret = write(socket, &data[i].id, sizeof(int))) == FAIL) 
        {
            printf("\n Failed to send data to socket\n");
            return FAIL;
        }
        
        if ((ret = write(socket, &data[i].start, sizeof(int))) == FAIL) 
        {
            printf("\n Failed to send data to socket\n");
            return FAIL;
        }
        if ((ret = write(socket, &data[i].end, sizeof(int))) == FAIL) 
        {
            printf("\n Failed to send data to socket\n");
            return FAIL;
        }
        
        for(k=0; k< data[i].numValue; ++k)
        {
			if ((ret = write(socket, &data[i].value[k], sizeof(double))) == FAIL) 
			{
				printf("\n Failed to send data to socket\n");
				return FAIL;
			}
			
		}
        
    }

    return SUCCESS;
}




