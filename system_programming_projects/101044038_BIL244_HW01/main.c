/*######################################################################
 * 
 * LECTURE NAME  :   BIL244 SYSTEM PROGRAMMING /  2014
 * 
 * HOMEWORK  1   :   USAGE : ./grepM  -filename "string"
 *                   Search string and find number of string in file
 * 
 * STUDENT       :   FATMA CELIK - 101044038
 *
 * DATE          :   11 MARCH 2014 / TUESDAY
 * 
######################################################################*/

#include "grepM.h"

/***********************************************************************
 * Function name : main(int argc, CHARPTRPTR argv)
 * 
 * Description   : calls Start() function to run program
 * 
 * Parameters    : (integer) argc is number of arguments ,
 * 				   (two dimensional character array) argv holds arguments
 *
 * Return        :  return 0
 * 
************************************************************************/
int main(int argc, CHARPTRPTR argv)
{
	CHARPTR s1 = "\n*****************************************"
	   "************************************\n";
	   
	/*  checked arguments */
	if( Usage(argc,argv)  == SUCCESS)
	{
		Start(&argv[1][1],argv[2]);
	}

	fprintf(stdout,"%s",s1);
    return 0;
}


/***********************************************************************
 * Function name : Usage(int argc, CHARPTRPTR argv)
 * 
 * Description   : Check of arguments to entered from terminal
 * 
 * Parameters    : (integer) argc is number of arguments ,
 * 				   (two dimensional character array) argv holds arguments
 *
 * Return        : If arguments were entered correct, return SUCCESS(1)
 * 				   otherwise return ERROR(0)
 * 
************************************************************************/
int Usage(int argc, CHARPTRPTR argv)
{
	FILEPTR inPtr;
	int i;
	CHARPTR s1 = "\n*****************************************"
				 "************************************\n";
	   
	CHARPTR mes =
	   "\n USAGE : ./grepM  -filename \"string\" "
	   "\n\n There must be three arguments, respectively: "
	   "\n ./grepM    ->  execution name"
	   "\n -filename  ->  input file (.txt) and must "
	   "be '-' symbol in front of filename"
	   "\n \"string\"   ->  search string  between two (\") symbol \n"
	   "\n Please enter only one filename and one string!\n";

	/*---------------------------------------------------------------*/
	CLEAR_SCREEN;

	fprintf(stdout,"%s\n You entered ->",s1);
	for(i=0; i<argc;++i)
		fprintf(stdout," %s ",argv[i]);
	fprintf(stdout,"\n");
	   
	/*---------------------------------------------------------------*/
	/* check number of arguments*/
	if( argc != 3 )
	{
		fprintf(stdout,"\n Please enter three arguments!!\n%s%s",s1,mes);
		return ERROR;
	}
	/*---------------------------------------------------------------*/
	/* check execution name */
	if(strcmp(argv[0],"./grepM") != 0)
	{
		fprintf(stdout,"\n You entered incorrect entry. "
					   "\n Please check execution name!!\n%s%s",s1,mes);
		return ERROR;			   
	}
	
	/*---------------------------------------------------------------*/
	/* check '-'  symbol in front of filename*/
	if(argv[1][0] != '-')
	{
		fprintf(stdout,"\n You entered incorrect entry. "
	    "\n Please check '-' symbol in front of filename!!\n%s%s",s1,mes);
	    return ERROR;
	}
	/*---------------------------------------------------------------*/
	else
	{
		/* check file opened? */
		inPtr = fopen(&argv[1][1],"r");
		
		if(inPtr == NULL)
		{
			fprintf(stdout,"\n \"%s\" not opened! "
			"\n Please check input file!!\n%s%s",&argv[1][1],s1,mes);
			return ERROR;
		}

		fclose(inPtr);
	}
	/*---------------------------------------------------------------*/

	
	return SUCCESS;
}
