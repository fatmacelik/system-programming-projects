/*######################################################################
 * 
 * LECTURE NAME  :   BIL244 SYSTEM PROGRAMMING /  2014
 * 
 * HOMEWORK  1   :   USAGE : ./grepM  -filename "string"
 *                   Search string and find number of string in file
 * 
 * STUDENT       :   FATMA CELIK - 101044038
 *
 * DATE          :   11 MARCH 2014 / TUESDAY
 * 
######################################################################*/

#include "grepM.h"


/***********************************************************************
 * Function name : GrepM(char *fileName, int *sizeOfLine,
 * 										 int lineLen, CHARPTR target)
 * 
 * Description   : Search target in file and save number of target in file 
 * 
 * Parameters    : input file name,
 * 				   number of characters in line(size of line),
 * 				   number of line in file,
 * 				   search string target			   
 *
 * Return        : void
 * 
************************************************************************/
void GrepM(CHARPTR fileName, INTPTR sizeOfLine, int numOfLine, CHARPTR target)
{
	FILEPTR fPtr,outPtr;
	CHARPTRPTR str;
	int i,count=0,sum=0;

    if ((fPtr = fopen(fileName,"r")) == NULL || (outPtr = fopen("screen.txt","w")) == NULL)
    {
		fprintf(stdout,"\n Not open file. (Function name : GrepM)\n");
		fclose(fPtr); /* close file*/
		fclose(outPtr); /* close file*/
		exit(ERROR);
	}
    else
    {
		/* malloc for  2d char array*/
		str = (char**)malloc(sizeof(char*) * numOfLine);
		if( str != NULL)
		{		
			for(i=0; i<numOfLine;++i)
			{
				/*malloc to read string  for i. line*/
				str[i] = (char*)malloc(sizeof(char) * ( sizeOfLine[i] * 2) );
				if(str[i] != NULL )
				{
					/*read string  i. line */
					fgets(str[i], sizeof(char*) * (sizeOfLine[i] * 2), fPtr);
					
					/* count is number of target in str[i]*/
					/*search target in string str[i]*/
					count = Search(str[i],target);

					fprintf(stdout,"\n Line %5d : Number of  \"%s\" : %d ",i+1,target,count);
					fprintf(outPtr,"\n Line %5d : Number of  \"%s\" : %d ",i+1,target,count);
					sum += count;/* sum of <number of target>*/
				}
				else
				{
					fprintf(stdout,"\n Malloc is failed in GrepM function.(str[%d])!! ",i);
					Free(str,i+1);
					fclose(fPtr); /* close file*/		
					exit(ERROR);
				}
			}/*end for*/

			fprintf(stdout,"\n\n Number of \"%s\"  in file = %d \n",target,sum);
			fprintf(outPtr,"\n\n Number of \"%s\"  in file = %d \n",target,sum);

		}/*end if*/
		else
		{
			fprintf(stdout,"\n Malloc is failed in GrepM function.(str)!! ");
			free(str);
			fclose(fPtr); /* close file*/
			fclose(outPtr); /* close file*/
			exit(ERROR);
		}/*end else*/ 
		

		fclose(fPtr); /* close file*/
		fclose(outPtr); /* close file*/
		Free(str,numOfLine);/* call free function for malloc*/

		
	}/*end else*/



}/*end function*/


/***********************************************************************
 * Function name : Search(CHARPTR str, CHARPTR target )
 * 
 * Description   : Search target and find number of target in str
 * 
 * Parameters    : two char array str and target			   
 *
 * Return        : int , return number of target in str
 * 
************************************************************************/
int Search(CHARPTR str, CHARPTR target )
{
	int count=0, status=0;
	int i;

	for(i=0; i< strlen(str) ; ++i)
	{
		status = 0;
		if( str[i] == target[0]) 
		{/* if target first character is found, call IsSubStr function*/
			status = IsSubStr(&str[i], target);

			if(status == 1)
				count++; /* if target is found, increase count*/
		}
	}

	return count;
}

/***********************************************************************
 * Function name : IsSubStr(CHARPTR str, CHARPTR target)
 * 
 * Description   : Control of target is sub string in str
 * 
 * Parameters    : two char array str and target			   
 *
 * Return        : int , return status if target is in str, return 1,
 * 				   otherwise return 0
 * 
************************************************************************/
int IsSubStr(CHARPTR str, CHARPTR target)
{
	int i;
	
	for( i=0; i<strlen(target); ++i)
	{
		if(target[i] != str[i])
			return 0;   /* letters not same, then target */
					    /*is not sub string, return false*/
	}


	return 1; /* if target is sub string of str, return true*/
}

/***********************************************************************
 * Function name : Free(CHARPTRPTR str ,int size)
 * 
 * Description   : free function for malloc 
 * 
 * Parameters    : 2D char array and size			   
 *
 * Return        : void
 * 
************************************************************************/
void Free(CHARPTRPTR str ,int size)
{
	int i;

	for(i=0; i< size; i++)
		free(str[i]);

	free(str);
}


/***********************************************************************
 * Function name : Start(CHARPTR fileName, CHARPTR target)
 * 
 * Description   : starting  all operation and call functions
 * 
 * Parameters    : input file name, searching array <target>		   
 *
 * Return        : void
 * 
************************************************************************/
void Start(CHARPTR fileName, CHARPTR target)
{
	/* variables */
    int numOfLine;
	INTPTR sizeOfLine;/* number of characters in a line in file*/

	/* number of lines in file*/
	numOfLine = FindNumberOfLine(fileName);
	
	/*control number of lines in file*/
	printf("\n Number of line in file: %d \n",numOfLine);	

	if( numOfLine > 0)
	{
		/* malloc for read datas from file*/
		sizeOfLine = (int*)malloc(sizeof(int) * numOfLine);
		if(sizeOfLine != NULL)
		{
			/*find number of characters in file and */
			/*save numer of chars in integer array sizeOfLine*/	
			FindNumOfChars(fileName,sizeOfLine);

			/* call GRepM function*/
			GrepM(fileName,sizeOfLine,numOfLine,target);

			free(sizeOfLine);		/*call free for malloc*/
					
		}
		else
		{
			fprintf(stdout,"\n Malloc is failed in Start function..\n");
			free(sizeOfLine);		/*call free for malloc*/		
			exit(ERROR);
		}

	}
	else
	{
		fprintf(stdout,"\n Input file is empty!!\n");
	}
}



/***********************************************************************
 * Function name : FindNumberOfLine(CHARPTR fileName)
 * 
 * Description   : find number of line in input file
 * 
 * Parameters    : input file name	   
 *
 * Return        : int, return number of lines in file
 * 
************************************************************************/
int FindNumberOfLine(CHARPTR fileName)
{
    FILE *fPtr;
    char c;
    int line = 0,count=0;

    if ((fPtr = fopen(fileName, "r")) == NULL)
    {		
        fprintf(stdout,"\n File not open in FindNumberOfLine function. \n");
        fclose(fPtr); /* close file */
		exit(ERROR);
    }
    else
    {       
		do{
			c = fgetc(fPtr); /* read character in file*/

			if(c == '\n')
				count++; /* increase line number*/
				
		}while(c != EOF);

		line = count; /* number of line in file*/

        fclose(fPtr); /* close file */
    }

    return line;
}



/***********************************************************************
 * Function name : FindNumOfChars(CHARPTR filename,INTPTR lines)
 * 
 * Description   : find number of characters in a line
 * 
 * Parameters    : input file name, integer array lines	   
 *
 * Return        : void
 * 
************************************************************************/
void FindNumOfChars(CHARPTR fileName,INTPTR lines)
{
    FILE *fPtr;
    char c;
    int line = 0,count=0;

    if ((fPtr = fopen(fileName, "r")) == NULL)
    {
		fprintf(stdout,"\n Not open file in FindNumOfChars fnction.\n");
		fclose(fPtr);/*close file*/
		exit(ERROR);
    }
    else
    {       
		do{
			c = fgetc(fPtr);
			count++;

			if(c == '\n')
			{
				/* save number of characters in a line*/
				lines[line]=count;
				line++;/* increase line index*/
				count=0;
			}
								
		}while(c != EOF);

		
		fclose(fPtr);/*close file*/
    }
		
    
}


