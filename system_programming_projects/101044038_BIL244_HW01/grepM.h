/*######################################################################
 * 
 * LECTURE NAME  :   BIL244 SYSTEM PROGRAMMING /  2014
 * 
 * HOMEWORK  1   :   USAGE : ./grepM  -filename "string"
 *                   Search string and find number of string in file
 * 
 * STUDENT       :   FATMA CELIK - 101044038
 *
 * DATE          :   11 MARCH 2014 / TUESDAY
 * 
######################################################################*/

#ifndef GREP_H
#define GREP_H

	/************************** INCLUDES *********************************/
	#include<stdio.h>   /* printf()           */
	#include<string.h>  /* strcmp(), strlen() */
	#include<stdlib.h>  /* malloc()           */

	/************************** DEFINES **********************************/

	#define ERROR 0
	#define SUCCESS 1
	#define CLEAR_SCREEN system("clear")

	/************************** TYPEDEFS *********************************/
	typedef int* INTPTR; 		/* integer pointer       */
	typedef char* CHARPTR;      /* char pointer          */
	typedef char** CHARPTRPTR;  /* two dimensional array */
	typedef FILE* FILEPTR;      /* FILE pointer          */


	/************************** FUNCTIONS ********************************/
	/* fin number of lines in input file*/
	int  FindNumberOfLine(CHARPTR fileName);
	
	/*  check of target is sub string of str*/
	int  IsSubStr(CHARPTR str, CHARPTR target);
	
	/* search target in string str*/
	int  Search(CHARPTR str, CHARPTR target);
	
	/* program usage, print on screen message */
	int  Usage(int argc, CHARPTRPTR argv);

	/* program starting function*/
	void Start(CHARPTR fileName, CHARPTR target);
	
	/* find number of characters in a line in input file*/
	void FindNumOfChars(CHARPTR fileName, INTPTR  lines);
	
	/* grep function, call search function and search target in file*/
	void GrepM(CHARPTR fileName, INTPTR sizeOfLine, int numOfLine, CHARPTR target);
	
	/* deallocate - free function for two dimensional array*/
	void Free(CHARPTRPTR str, int size);

#endif
