/*#################################################################*/
/*
* SYSTEM PROGRAMMING - ODEV 4 - ADD THREAD INTO VIZE PROGRAM
*
* Author 		: FATMA CELIK 
* Student No    : 101044038    
*                                                  
* Date		    :  April 29, 2014/                                           
*                                                         
*/         
/*##################################################################*/




#include "types.h"


static int error_location=ERROR_LOCATION; /* parse error index kaydedilir*/
char errorMes[SIZE]=""; /* parse error mesaji kaydedilir*/



int doParseFunction(CHARPTR function, CHARPTR operatorArray, OPERATIONSPTR operations)
{
	int i,opIndex=0,indexOfFunc=0;
	char operator=' ';
	int status=ERROR_LOCATION;
	
	if( function[0] != '[')
	{
		error_location=0;
		strcpy(errorMes,"Missing '['  Opening Brackets");
		status=error_location;
	}

	/* fonksiyon ve operator bilgileri belirlenir*/
	/* setFunction ve setOperator fonckyionları cagrilir*/
	for(i = 0;i < strlen(function) && status == ERROR_LOCATION; ++i	)
	{			
		if(function[i] == '[')/* fonksiyon baslangic parantezi*/
		{
			status=setFunction(function,i,&operations[indexOfFunc]);			
			indexOfFunc++;							
		}
		else if( function[i] == ']') /* fonksiyon bitisi*/
		{
			if(function[i+1] == ' ')/* operator aramasi icin baslangic */
			{
				status=setOperator(function,i+1,&operator);
				operatorArray[opIndex]=operator;
				++opIndex;
			}
			else if(function[i+1] != '\0') 
			{
				error_location=i+1;
				status=error_location;
				strcpy(errorMes,"Invalid Character or Missing a blank");
			}
		}
	}/* end for */
	
	if(status != ERROR_LOCATION){ /* error control*/
		printParseFormat(function);
		return PARSE_ERROR;
	}
	
	return PARSE_OK;
}









int setFunction(CHARPTR function, int index, OPERATIONSPTR operation)
{
	int status=ERROR_LOCATION;
	int last;

	if(function[index] == '[')
	{
		last=index+1;	
		operation->name='a';
		status=saveCoef(function,&last,'*',operation);
		if(status != ERROR_LOCATION)
			return status;
		
		if(function[last+1] == 's')  /* sin dont have coefficient*/
		{			
			(*operation).name='s';
			status=parseSinCos(function,last+1,operation);
		}
		else if(function[last+1] == 'c') /* cos dont have coefficient*/
		{
			(*operation).name='c';
			status=parseSinCos(function,last+1,operation);
		}
		else if(function[last+1] == 't')  /*  dont have coefficient*/
		{
			(*operation).name='t';
			(*operation).coef=(*operation).trigoCoef;		
			(*operation).trigoCoef=1;
			status=parseT(function,last+1,operation);
		}
		else
		{
			error_location=last+1;
			strcpy(errorMes,"Invalid character or Missing a character in here");
			return error_location;
		}
	}
	else
	{
		error_location=index;
		strcpy(errorMes,"Invalid character or Missing '[' opening brackets in here");
		return error_location;
	}

	return status;
}


int parseT(CHARPTR function, int index, OPERATIONSPTR operation)
{
	int status=ERROR_LOCATION;
	int tempError,last;
	
	if(function[index] == 't' && function[index+1] == '^'&& function[index+2] == '(' )
	{
		last=index+3;
		status=saveCoef(function,&last,')',operation);
		if(status != ERROR_LOCATION)
			return status;

		if(function[last+1] != ']')
		{
			error_location=last+1;
			strcpy(errorMes,"Invalid character or Missing ']' closing brackets in here");
			return error_location;
		}
	}
	else
	{
		if(function[index] != 't')
		{
			tempError=index;
			strcpy(errorMes,"Invalid character or Missing 't' character in here");
		}
		else if(function[index+1] != '^')
		{
			tempError=index+1;
			strcpy(errorMes,"Invalid character or Missing '^' character in here");
		}
		 else if(function[index+2] != '(')
		{
			tempError=index+2;
			strcpy(errorMes,"Invalid character or Missing '(' opening parenthesis in here");
		}

		error_location=tempError;

		return error_location;

	}
	


	return status;
}


int parseSinCos(CHARPTR function, int index, OPERATIONSPTR operation)
{
	char first,mid,last;
	int tempError;
	int status;


	first = function[index];	
	/* set sinus or cosinus characters*/
	if(first == 's')
	{
		mid='i';
		last='n';
	}else if(first=='c')
	{
		mid='o';
		last='s';
	}
	
	
	/* control of sin or cos name and '(' parantez */
	if(	function[index] == first &&
		function[index+1] == mid &&
		function[index+2] == last &&
		function[index+3] == '(')
	{
		/* save function coef and exponent*/			
		status = saveSinCosElems(function,index+3,operation);						
		if(status != ERROR_LOCATION)
			return status;
	}
	else
	{
		/* set error location*/	
		if(function[index] != first)
		{
			tempError=index;
			strcpy(errorMes,"Invalid character or Missing a character in here");
		}
		else if(function[index+1] != mid)
		{
			tempError=index+1;
			strcpy(errorMes,"Invalid character or Missing a character in here");
		}
		else if(function[index+2] != last)
		{
			tempError=index+2;
			strcpy(errorMes,"Invalid character or Missing a character in here");
		}
		else if(function[index+3] != '(')
		{
			tempError=index+3;
			strcpy(errorMes,"Invalid character or Missing '(' opening parenthesis in here");
		}
		error_location=tempError;
		return tempError;
	}				
	

	return ERROR_LOCATION;
}

int saveCoef(CHARPTR function, INTPTR last,char ch,OPERATIONSPTR operation)
{
	int status=ERROR_LOCATION;
	int neg=0,tempIndex=0;
	char temp[SIZE]="";
	
	neg=0;
		
	if(function[*last] == '-')
	{	
		neg =1;	
		++(*last);
	}

	if(isNumber(function[*last]) == NOT_NUMBER )
	{
		error_location=*last;
		strcpy(errorMes,"Invalid character or Missing a number in here.");
		return error_location;
	}

	tempIndex=0;
	temp[0]='\0';
	status=setCoef(function, last,temp,&tempIndex,ch);

	if(status != ERROR_LOCATION)
		return status;


	temp[tempIndex]='\0';

	if(operation->name == 'a' && ch == '*')
		(*operation).trigoCoef= atof(temp);
	else if(ch == ')')
		(*operation).exponent= atof(temp);
	else if(ch == '*')
		(*operation).coef= atof(temp);
		
	if(neg==1)// is negative
	{
		if(operation->name == 'a' && ch == '*')
			operation->trigoCoef= 0.0 - operation->trigoCoef;		
		if(ch == ')')
			operation->exponent= 0.0 - operation->exponent;
		else if(ch == '*')
			operation->coef= 0.0 - operation->coef;
		neg=0;
	}

	return status;

}


int saveSinCosElems(CHARPTR function, int index, OPERATIONSPTR operation)
{
	int tempError,tempLast;
	int last; //index
	
	int status=ERROR_LOCATION;
	
	/*----------------------------------------------------------*/
	last=index+1;	
	status=saveCoef(function,&last,'*',operation);
	if(status != ERROR_LOCATION)
		return status;

	/*----------------------------------------------------------*/	
	if( function[last] == '*' && function[last+1] == 'w'  && function[last+2] == '*' &&
		function[last+3] == 't'  && function[last+4] == '^' && function[last+5] == '(')

	{
		tempLast=last+6;
		status=saveCoef(function,&tempLast,')',operation);
		if(status != ERROR_LOCATION)
			return status;
		
		if(function[tempLast+1] != ')' )
		{
			error_location=tempLast+1;
			strcpy(errorMes,"Invalid character or Missing ')' closing parenthesis in here");
			return error_location;
		}				
		if(function[tempLast+2] != ']')
		{
			error_location=tempLast+2;
			strcpy(errorMes,"Invalid character or Missing ']' closing brackets in here");
			return error_location;
		}
		
	}	/* ERRORS*/
	else
	{
		/* set error location*/
		if(function[last] != '*')
		{
			tempError=last;
			strcpy(errorMes,"Invalid character or Missing '*' character in here");
		}
		else if(function[last+1] != 'w')
		{
			tempError=last+1;
			strcpy(errorMes,"Invalid character or Missing 'w' character in here");
		}	
		else if(function[last+2] != '*')
		{
			tempError=last+2;
			strcpy(errorMes,"Invalid character or Missing '*' character in here");
		}
		else if(function[last+3] != 't')
		{
			tempError=last+3;
			strcpy(errorMes,"Invalid character or Missing 't' character in here");
		}	
		else if(function[last+4] != '^')
		{
			tempError=last+4;
			strcpy(errorMes,"Invalid character or Missing '^' character in here");
		}	
		else if(function[last+5] != '(')
		{
			tempError=last+5;
			strcpy(errorMes,"Invalid character or Missing '(' opening parenthesis in here");
		}
			
		error_location=tempError;
		return tempError;
	}

	
	return ERROR_LOCATION;
}




int setCoef(CHARPTR function, INTPTR last,CHARPTR temp,INTPTR tempIndex,char ch)
{
	int k,counter=0;

	for(k=*last; function[k] != ch && function[k] != '\0'; ++k)
	{
		if( isNumber( function[k] ) == IS_NUMBER )
		{
			temp[*tempIndex]=function[k];
			++(*tempIndex);
		}
		else if (function[k] == '.')
		{
			temp[*tempIndex]=function[k];
			++(*tempIndex);
			counter++;

			if(counter==2) // just have only  '.'
			{
				error_location=k;
				strcpy(errorMes,"Invalid character. (Multiple dot points.)");
				return error_location;
			}
			
			if( isNumber(function[k+1]) == NOT_NUMBER)
			{
				error_location=k+1;
				strcpy(errorMes,"Invalid character or Missing a number in here.");
				return error_location;
			}
		}
		else{
			error_location=k;
			strcpy(errorMes,"Invalid Character or Missing '*' character in here");
			return error_location;
		}
		
	}/* for end*/
	
	*last=k;

	return ERROR_LOCATION;

}







int setOperator(CHARPTR function, int index, CHARPTR operator) 
{
	if(	function[index] == ' '  && isOperator(function[index+1]) == IS_OPERATOR &&
		function[index+2] == ' '  && function[index+3] == '[')
	{
		*operator=function[index+1];
	}
	else
	{
		if(function[index] != ' ')
		{
			error_location=index;
			strcpy(errorMes,"Invalid character or Missing a blank");
			return error_location;
		}
		else if(isOperator(function[index+1]) != IS_OPERATOR)
		{
			error_location=index+1;
			strcpy(errorMes,"Invalid character or Missing a operator");
			return error_location;
		}
		else if(function[index+2] != ' ')
		{
			error_location=index+2;
			strcpy(errorMes,"Invalid character or Missing a blank");
			return error_location;
		}
		else if(function[index+3] != '[')
		{
			error_location=index+3;
			strcpy(errorMes,"Invalid character or Missing '[' Opening Brackets");
			return error_location;
		}
	}
	
	return ERROR_LOCATION; //return okey
}










void setNumOfOperator(CHARPTR function, INTPTR numOfOperator)
{
	int i;

	for(i=0; i < strlen(function) ; ++i)
	{				
		if( function[i] == ']' && function[i+1] != '\0' && function[i+2] != '\0' &&
			function[i+3] != '\0' && function[i+4] != '\0')
		{
			if(function[i+1] == ' ' && function[i+3] == ' ' && function[i+4] == '[')
			{
				if( function[i+2] == '+' || function[i+2] == '-' ||
					function[i+2] == '*' || function[i+2] == '/' )
				{
					(*numOfOperator)++;
				}	
			}		
		}/* if end --- check of null*/
	} /* end for */
}
	



void printParseFormat(CHARPTR function)
{
	int i;
	
	fprintf(stdout, "\n ****************\n"
					" YOU ENTERED:    %s  ",function);
	fprintf(stdout, "\n ****************\n "
					"\n PARSE ERROR RESULT: "
					"\n -------------------\n");
	
	fprintf(stdout,	"\n\"%s\"\n",function);
	for(i=0; i<error_location+1;++i)
		fprintf(stdout," ");
	fprintf(stdout,"^\n");

	for(i=0; i<error_location+1;++i)
		fprintf(stdout," ");
	fprintf(stdout,"Error location is here... index: %d \n",error_location);

	for(i=0; i<error_location+1;++i)
		fprintf(stdout," ");
	fprintf(stdout,"%s \n",errorMes);


	
	fprintf(stdout,
	"\n FUNCTION PARSE FORMAT MUST BE:"
	"\n|-----------------------------------------------------------|"
	"\n|                T   &   SINUS   &  COSINUS                 |"
	"\n|-----------------------------------------------------------|"
	"\n| [A*sin(B*w*t^(C))] * [A*t^(B)] + [D*cos(E*w*t^(F))]       |"
	"\n| [A*cos(B*w*t^(C))] - [A*cos(D*w*t^(E))]                   |"
	"\n| [A*t^(B)] / [A*cos(B*w*t^(C))] + [A*cos(B*w*t^(C))]       |"
	"\n|                                                           |"
	"\n| Coefficients: A , B , C , D , E , F                       |"
	"\n| Examples    : 4  5.77  -789.14  -50.1  47.8  -1  6.5 -5   |"
	"\n|                                                           |"
	"\n| Function is between opening '[' and ']' closing brackets  |"
	"\n|                                                           |"
	"\n| sin(),cos(), exponent of t^()  must have                  |"
	"\n|                        opening  and closing  parenthesis  |"
	"\n|                                                           |"
	"\n|-----------------------------------------------------------|"
	"\n|                    OPERATOR  FORMAT                       |"
	"\n|-----------------------------------------------------------|"
	"\n|  [A*sin(B*w*t^(C))] * [A*t^(B)] + [D*cos(E*w*t^(F))]      |"
	"\n|                                                           |"
	"\n|  There must be a blank character at                       |"
	"\n|                          right and left side of operator  |"
	"\n|-----------------------------------------------------------|\n\n");


}

void printTestParse(OPERATIONSPTR operation, int numOfOperation, CHARPTR operatorArray, int numOfOperator)
{
	int i;
	char temp[SIZE]="";

	fprintf(stdout,"\n PARSE RESULT: \n -----------------------\n");
	for(i=0; i < numOfOperation ; ++i)
	{
		if( operation[i].name == 't')
		{
			fprintf(stdout,"[%.3lf*t^(%.3lf)]",operation[i].coef,
												operation[i].exponent);			
		}
		else
		{
			if( operation[i].name == 's')
				strcpy(temp,"sin");
			else if(operation[i].name == 'c')
				strcpy(temp,"cos");

			fprintf(stdout,"[%.3lf*%s(%.3lf*2*3.14*t^(%.3lf))]",
									operation[i].trigoCoef,temp,
									operation[i].coef,operation[i].exponent);			
		}

		if(i != numOfOperation-1)
			fprintf(stdout," %c ",operatorArray[i]);
	
	}/*end for*/

	fprintf(stdout,"\n -----------------------\n");
}
