/*#################################################################*/
/*
* SYSTEM PROGRAMMING - ODEV 4 - ADD THREAD INTO VIZE PROGRAM
*
* Author 		: FATMA CELIK 
* Student No    : 101044038    
*                                                  
* Date		    :  April 29, 2014/                                           
*                                                         
*/         
/*##################################################################*/



#include "types.h"
#include <pthread.h>


pid_t childs[MAX_CHILD] = {0};
SChild childHistory[MAX_CHILD];
int pIndex=0;


CHARPTR charResource[2];
DOUBLEPTR dbResource[1];
OPERATIONSPTR opResource[1];



int fds=-1;
int timeClient=-1;
int serverTimes=-1;
int operationNum=-1;
int operatorNum=-1;
char name;


void InitChildHistory(void)
{
	int i;
	SChild temp;
	
	memset(&temp, 0,sizeof(SChild));
	
	for(i=0; i< MAX_CHILD ;++i)
	{
		childHistory[i] = temp;
	}
	
}

/* Signal Handler for specific signals
   Gets two parameters which are handler func and signal flag
   Return -1 on error , otherwise returns true */
int SetHandler( Handler hand , int sig )
{
    if( signal(sig, hand) == SIG_ERR )
        return ERROR;

    return SUCCESS;
}



void InterruptHandlerParent(int sigNo)
{
    char msg[] = "\n Server died with its childs :(\n\n";
	int i;	
	FILEPTR file=NULL;
	time_t rawtime;

	file = fopen("server.log","w");
	
	if(file != NULL)
	{
		time(&rawtime);
		fprintf(file,"Server.log created time: %s\n\n",ctime(&rawtime));
		for(i=0; i< pIndex ;++i)
		{
			if( childs[i] != EMPTY )
			{
				kill(childs[i], SIGINT);			
			}
			
			if( childHistory[i].pid != EMPTY )
			{
				fprintf(file, "Client Pid : %d\tRequest: %s\t"
								"\tConnection time: %s",
								 childHistory[i].pid,
								 childHistory[i].request,							 
								 ctime(&childHistory[i].conTime));
			}
		}	
		
		
		fprintf(file,"\n\nServer died with its childs :(\n");
		fclose(file);
		
	}
	else
	{
		fprintf(stdout,"\nError : Failed to create log file !\n");
	}
	
    write(STDOUT_FILENO , msg , sizeof(msg));  
    
    unlink("./doMathFifo");
    
    free(charResource[0]);
	free(charResource[1]);
	free(dbResource[0]);
	free(opResource[0]);
	
    exit(EXIT_FAILURE);      
}



int Start( CHARPTR function,CHARPTR operatorArray,int numOfOperator,
	       OPERATIONSPTR operations,int numOfOperation,
	       DOUBLEPTR results,CHARPTR tempOp,int times)
{
	const char MAIN_FIFO[] = "./doMathFifo";
    int fd=-1;
    pid_t serverPid;
	
	/*-----------------------------------------------------------------*/
	/* save adress of malloc arrays to free after control c handler*/
	charResource[0] = operatorArray;
	charResource[1] = tempOp;
	dbResource[0] = results;
	opResource[0] = operations;
	
	/*-----------------------------------------------------------------*/
	if( doParseFunction(function,operatorArray,operations) == PARSE_ERROR)
	{
		return PARSE_ERROR;
	}

	/* backup */
	fillOperators(tempOp,operatorArray,numOfOperator); /* only once*/

	/* print test parse result*/
	printTestParse(operations,numOfOperation,operatorArray,numOfOperator);
	serverPid=getpid();
	fprintf(stdout,	"\n ------------------------"
					"\n Server pid : %d\n\n ",serverPid);

	if (mkfifo(MAIN_FIFO, FIFO_PERMS) != ERROR)
	{				
		return doChild(serverPid, fd,results, operations,numOfOperation,
							operatorArray,numOfOperator, tempOp, times);	
	}else
		fprintf(stdout,"\n Server : Failed to create fifo.\n\n");


	return SUCCESS;
}



int doChild(pid_t serverPid,int fd, DOUBLEPTR results, OPERATIONSPTR operations,
			int numOfOperation,CHARPTR  operatorArray, int numOfOperator, 
			CHARPTR tempOp, int times)
{
	int i;
	char clientName[SIZE]="";
	const char MAIN_FIFO[] = "./doMathFifo";
	int pid_client = 0, old_pid = 0,pid_server;
	int time_client;
	int fd2=-1;
	time_t rawtime;
	//int pIndex=0;
	char connectTime[SIZE]="";
	pthread_t myThread;;
	char intLog[SIZE]="",turevLog[SIZE]="";	
	char childFifo[PID_LEN] = "";

	
	for(i=1; i>0 ; ++i)
	{			
		if ((fd = open(MAIN_FIFO, O_RDONLY )) != ERROR)
		{					
			read(fd, &pid_server, sizeof (int));	
			//fprintf(stdout, "\n Pid From Client : %d\n", pid_server);
	
			if(pid_server == serverPid)
			{
				if (read(fd, &pid_client, sizeof (int))!= ERROR)					
					fprintf(stdout, "\n Pid From Client  : %d", pid_client);
																	
				if (read(fd, &time_client, sizeof (int)) != ERROR )						
					fprintf(stdout, "\n Time From Client : %d\n", time_client);
				
				if (read(fd, clientName, SIZE) != ERROR )
				{
					fprintf(stdout, " Name From Client : %s\n", clientName);
					if (old_pid != pid_client )
					{									
						sprintf(childFifo, "%d.fifo", pid_client);									
						
						timeClient=time_client;		
						serverTimes=times;			
						operationNum=numOfOperation;
						operatorNum=numOfOperator;	
						name = clientName[0];
								
						time(&rawtime);
						if(name == 'i')
						{
							strcpy(childHistory[pIndex].request,"integral");
							strcpy(intLog,"./integral");
							strcat(intLog,childFifo);
							
							if ((fd2 = open(intLog, O_WRONLY )) == ERROR)
							{	
								fprintf(stdout,"\n Server : Failed to open <%s> fifo.\n",intLog);
								//return ERROR;
							}
							
							sprintf(connectTime," Connection Time: %s\n Value%20sTimes ms\n",ctime(&rawtime)," ");
							if (write(fd2, connectTime, SIZE) == ERROR)
							{
								fprintf(stdout, "\n Server : Failed to write connect time into <%s> fifo", intLog);
								//return ERROR;
							}
						}
						else
						{
							strcpy(childHistory[pIndex].request,"turev");
							strcpy(turevLog,"./turev");
							strcat(turevLog,childFifo);
							
							if ((fd2 = open(turevLog, O_WRONLY)) == ERROR)
							{	
								fprintf(stdout,"\n Server : Failed to open <%s> fifo.\n",turevLog);
								//return ERROR;
							}
							
							sprintf(connectTime," Connection Time: %s\n Value%20sTimes ms\n",ctime(&rawtime)," ");
							if (write(fd2, connectTime, SIZE) == ERROR)
							{
								fprintf(stdout, "\n Server : Failed to write connect time into <%s> fifo", turevLog);
								//return ERROR;
							}							
						}	
						
						fds=fd2;
						
						
						childs[ pIndex ] = pid_client; 		
						time(&rawtime);
						childHistory[pIndex].pid = pid_client;																	
						childHistory[pIndex].conTime = rawtime;
						pIndex++;		
						
						
						/*########## thread created            ########*/
						/*##########  call <doMathThread> function ####*/
						/*##########  and change value 'i'     ########*/			
						if(pthread_create(&myThread, NULL, doMathThread, &i)) 
						{
							fprintf(stderr, "\n Server : Error creating thread\n");
							return ERROR;
						}												
												
						/* waiting other thread*/
						if(pthread_join(myThread, NULL))
						{
							fprintf(stderr, "\n Server : Error thread joining..\n");
							return ERROR;
						}																	
					}
					
					old_pid = pid_client;			
					if (close(fd) == ERROR)
					{
						fprintf(stdout,"\n Server : Failed to close  fifo.\n");	
						return ERROR;
					}
				}//read

			}/* pid server != serverPid  */ //no message	 
	
		}else
		{
			fprintf(stdout,"\n Server : Failed to open main fifo.\n");	
			return ERROR;
		}			
	}//for end	
	
	
	return SUCCESS;
}


/*######################   THREAD FUNCTION  ########################*/
void *doMathThread(void *i)
{
	int *k= (int *)i;
	char intLog[SIZE]="",turevLog[SIZE]="";
	struct timeval start,end;
	long timeDiff=0;
	double f1=0,f2=0,integral=0.0,turev=0.0;
	double tempTime=0;

				
			
	gettimeofday( &start , NULL );
	for(; *k< timeClient * MILLION ; (*k)++)
	{								
		if( (*k%serverTimes) == 0)
		{
			/*
			charResource[0] = operatorArray;
			charResource[1] = tempOp;
			dbResource[0] = results;
			opResource[0] = operations;*/
			
			f1 = evaluateFunc(dbResource[0], opResource[0],operationNum,charResource[0],operatorNum,*k );
			fillOperators(charResource[0],charResource[1],operatorNum);/* replacement */
			f2 = evaluateFunc(dbResource[0], opResource[0],operationNum,charResource[0],operatorNum,*k+H );
			fillOperators(charResource[0],charResource[1],operatorNum);/* replacement */

			turev = calcTurev(f2,f1);
			integral = calcIntegral(f2,f1);
			
			gettimeofday( &end , NULL );
			timeDiff = MILLION * ( end.tv_sec - start.tv_sec ) +
								 ( end.tv_usec -start.tv_usec );
				
			tempTime = (double)timeDiff;			 
			if( name == 'i')
			{				
				if (write(fds, &integral, sizeof (double)) == ERROR)
					fprintf(stdout, "\n Server : Failed to write integral result into <%s> fifo", intLog);			
				if (write(fds, &tempTime, sizeof (double)) == ERROR)				
					fprintf(stdout, "\n Server : Failed to write  timeDiff into <%s> fifo", intLog);				
			}
			else
			{
				if (write(fds, &turev, sizeof (double)) == ERROR)				
					fprintf(stdout, "\n Server : Failed to write turev result into <%s> fifo", turevLog);				
				if (write(fds, &tempTime, sizeof (double)) == ERROR)				
					fprintf(stdout, "\n Server : Failed to write  timeDiff into <%s> fifo", turevLog);										
			}					
		}		
	}//for end
		
	if (close(fds) == ERROR)
		fprintf(stdout,"\n Server : Failed to close  fifo.\n");	

	return NULL;
}











/*#######################  MATH FUNCTIONS  ##########################*/
double calcTurev(double f2, double f1)
{
	return ( (f2-f1) / H);

}
double calcIntegral(double f2, double f1)
{
	return H * ((f2 + f1)/2.0);
}

double evaluateFunc(DOUBLEPTR results, OPERATIONSPTR operation, int numOfOperation,
                    CHARPTR operatorArray, int numOfOperator, int value)
{
	int i;
	double calcResult=0.0;

	for(i=0;i<numOfOperation;++i)
		results[i] = calculate(operation[i],value);

	if(numOfOperator>0)
		calcResult = resultsCombine(results,operatorArray,numOfOperator);
	else
		calcResult = results[0];

	return calcResult;

}

double resultsCombine(DOUBLEPTR results, CHARPTR operatorArray, int numOfOperator)
{
	double allResult=0.0,left,right;
	int i,k;
	char ops[]="/*+-";
	
	int newOperator= numOfOperator,
		newOperation= numOfOperator+1;

	for(i=0; i < strlen(ops); ++i )
	{	
		for(k=0; k<newOperator ;++k)
		{
			if(operatorArray[k] == ops[i])
			{			
				left = results[k];
				right= results[k+1];		
				allResult = result(left,right,ops[i]);
				results[k] = allResult;
				operatorArray[k] = operatorArray[k+1];
				moveResult(results,k+1,&newOperation);
				moveOperator(operatorArray,k+1,&newOperator);
			}
		}/*for end*/
	}/* for end*/	

	return allResult;
}

void moveResult(DOUBLEPTR results,int first,INTPTR last)
{
	int i;

	if( (*last-1-first) == 1)
	{
		results[first] = results[*last-1];
		results[*last-1] = -9876.0;
	}
	else if(*last-1-first > 1)
	{
		for(i=first ; i < *last-1  ; ++i)
			results[i] = results[i+1];

		results[*last-1] = -9876.0;
	}
	(*last)--;
	
}

void moveOperator(CHARPTR operatorArray,int first, INTPTR last)
{
	int i;

	if( (*last-1-first) == 1)
	{
		operatorArray[first] = operatorArray[*last-1];
		operatorArray[*last-1] = '!';
	}
	else if ( (*last-1-first) > 1)
	{
		for(i=first ; i < *last-1  ; ++i)
			operatorArray[i] = operatorArray[i+1];

		operatorArray[*last-1] = '!';

	}
	
	(*last)--;
}




double result(double left, double right, char op)
{
	if(op == '/')
	{
		if(right < 0.000 || right > 0.000) // zero control
			return left / right;
		else
		{
			return left / 1.0;
		}
	}
	else if(op == '*')
		return left * right;
	else if(op == '+')
		return left + right;
	else //if (op == '-')
		return left - right;

}



/* calculate sinus, cosinus and exponent of t*/
double calculate(OPERATIONS operation,int value)
{
	double result=0.0;
	
	if(operation.name == 's')
	{
		result =  (operation.trigoCoef * sin( operation.coef * W * pow(value, operation.exponent)));
	}
	else if(operation.name == 'c')
	{
		result =  (operation.trigoCoef * cos( operation.coef * W * pow(value, operation.exponent)));
	}
	else if(operation.name == 't')
	{
		result =  operation.coef * pow(value, operation.exponent);
	}
	
	return result;
}


/* copy elements to array1 from array2*/
void fillOperators(CHARPTR arr1, CHARPTR arr2 , int numOfOperator)
{
	int i;

	for(i=0; i<numOfOperator ; ++i)
		arr1[i]= arr2[i];
}
