/*#################################################################*/
/*
* SYSTEM PROGRAMMING - ODEV 4 - ADD THREAD INTO VIZE PROGRAM
*
* Author 		: FATMA CELIK 
* Student No    : 101044038    
*                                                  
* Date		    :  April 29, 2014/                                           
*                                                         
*/         
/*##################################################################*/



#ifndef TYPES_H
#define TYPES_H

/*######################   INCLUDES ################################ */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <errno.h>
#include <dirent.h>
#include <wait.h>
#include <ctype.h>    
#include <termios.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h> 
#include <time.h> 

/*######################   DEFINES ################################ */


#define CLEAN_SCREEN system("clear")

#define USAGE_ERROR -1

#define SUCCESS 1

#define MALLOC_FAIL -1
#define PID_LEN 20

#define IS_NUMBER 1
#define NOT_NUMBER 0

#define IS_OPERATOR 1
#define NOT_OPERATOR 0

#define PARSE_ERROR 0
#define PARSE_OK 1

#define ERROR_LOCATION -1

#define ERROR -1
#define EMPTY 0
#define SIZE 100

#define PI 3.14
#define W  (2 * PI)

#define H 5

#define MILLION 1000000L

#define MAX_CHILD 20

#define FIFO_PERMS (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

/*######################   TYPEDEFS  ################################ */

/* fonksiyon katsayilarinin tutuldugu struct yapisi*/
typedef struct Operations
{
	char name;   		/* sin: s,  cos: c,  t: t        */
	double coef; 		/* coefficient of t              */
	double trigoCoef;   /* coefficient of sin or cos     */
	double exponent; 	/* exponent of base. (t)         */
}operations_t;


typedef struct
{
	pid_t pid;
	char request[PATH_MAX];
	time_t conTime;	
}SChild;


typedef FILE*			FILEPTR;
typedef char*  			CHARPTR;
typedef char** 			CHARPTRPTR;
typedef int*   			INTPTR;
typedef int**  			INTPTRPTR;
typedef double* 		DOUBLEPTR;
typedef double** 		DOUBLEPTRPTR;
typedef operations_t* 	OPERATIONSPTR;
typedef operations_t 	OPERATIONS;
typedef void (*Handler)(int);/* Signal handler */


/*###############  PARSE FUNCTIONS PROTOTYPES ################## */
void printTestParse(OPERATIONSPTR operation, int numOfOperation,
					CHARPTR operatorArray,   int numOfOperator);
				
void printParseFormat(CHARPTR function);
void setNumOfOperator(CHARPTR function, INTPTR numOfOperator);
int doParseFunction(CHARPTR function, CHARPTR operatorArray, OPERATIONSPTR operations);
int saveSinCosElems(CHARPTR function, int index, OPERATIONSPTR operation);
int setOperator(CHARPTR function, int index, CHARPTR operator) ;
int setFunction(CHARPTR function, int index, OPERATIONSPTR operation);
int parseSinCos(CHARPTR function, int index, OPERATIONSPTR operation);
int saveCoef(CHARPTR function, INTPTR last,char ch,OPERATIONSPTR operation);
int setCoef(CHARPTR function, INTPTR last,CHARPTR temp,INTPTR tempIndex,char ch);
int parseT(CHARPTR function, int index, OPERATIONSPTR operation);

/*################  HELPER FUNCTIONS PROTOTYPES ############### */
int checkDoMathsArguments(int argc, CHARPTRPTR argv);
int isNumber(char ch);
int isOperator(char op);
int isNumber(char ch);


/*################# MATH FUNCTIONS PROTOTYPES ################### */

/* control c handler functions*/
int SetHandler( Handler hand , int sig );
void InterruptHandlerParent(int sigNo);
void InitChildHistory(void);


/* math functions*/
double evaluateFunc(DOUBLEPTR results,  OPERATIONSPTR operation,
					int numOfOperation, CHARPTR operatorArray,
					int numOfOperator,  int value);
                                    
double resultsCombine(DOUBLEPTR results,CHARPTR  operatorArray, int numOfOperator);

double calculate(OPERATIONS operation, int value);
double result(double left, double right, char op);
void moveResult(DOUBLEPTR results,int first,INTPTR last);
void moveOperator(CHARPTR operatorArray,int first, INTPTR last);

void fillOperators(CHARPTR arr1, CHARPTR arr2 , int numOfOperator);
double calcTurev(double f2, double f1);
double calcIntegral(double f2, double f1);

/* ************** */
int Start(	CHARPTR function,CHARPTR operatorArray,int numOfOperator,
			OPERATIONSPTR operations,int numOfOperation,DOUBLEPTR results,
			CHARPTR tempOp, int time);

int doChild(pid_t serverPid,int fd, DOUBLEPTR results, OPERATIONSPTR operations,
			int numOfOperation,CHARPTR  operatorArray, int numOfOperator, 
			CHARPTR tempOp, int times);

void *doMathThread(void *i);


#endif
