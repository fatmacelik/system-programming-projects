/*#################################################################*/
/*
* SYSTEM PROGRAMMING - ODEV 4 - ADD THREAD INTO VIZE PROGRAM
*
* Author 		: FATMA CELIK 
* Student No    : 101044038    
*                                                  
* Date		    :  April 29, 2014/                                           
*                                                         
*/         
/*##################################################################*/



#include "types.h"


/***********************************************************************
 * Function name : int main(int argc, CHARPTRPTR argv)
 * 
 * Description   :  program test fonksiyonu
 * 
 * Parameters    :  arguman sayisi(integer argc)
 * 					argumanlar string'i( char** argv)	   
 *
 * Return        :  success, 0
 * 
************************************************************************/
int main(int argc, CHARPTRPTR argv)
{
    CHARPTR operatorArray,tempOp;
    int numOfOperator=0,numOfOperation=0;
    OPERATIONSPTR operations;
    DOUBLEPTR results;

    InitChildHistory();
    /* Handler for the parent. ctrl + c */
    if( SetHandler( InterruptHandlerParent , SIGINT) == ERROR )
    {
        fprintf(stdout,"\n Failed to set signal handlers.\n\n");
        return ERROR;
    }
    
    /*-----------------------------------------------------------------*/
    if( checkDoMathsArguments(argc,argv) == USAGE_ERROR)
    {
		return USAGE_ERROR;
    }
    
    /*-----------------------------------------------------------------*/
    setNumOfOperator(argv[1],&numOfOperator);
    operatorArray = (CHARPTR)malloc(sizeof(char) * numOfOperator);
    tempOp = (CHARPTR)malloc(sizeof(char) * numOfOperator);
    if(operatorArray == NULL || tempOp == NULL)
    {
		free(operatorArray);
		free(tempOp);
		fprintf(stdout,"\n Operator Array malloc failed.\n");
		return MALLOC_FAIL;
    }
    /*-----------------------------------------------------------------*/	
    numOfOperation = numOfOperator+1; /* set number of operations */
    operations = (OPERATIONSPTR)malloc(numOfOperation * sizeof(OPERATIONS));	
    if(operations == NULL)
    {
		free(operatorArray);
		free(operations);
		free(tempOp);
		fprintf(stdout,"\n Operations struct malloc failed.\n");
		return MALLOC_FAIL;
    }

    /*-----------------------------------------------------------------*/	
    results = (DOUBLEPTR)malloc(sizeof(double) * numOfOperation);
    if(results == NULL )
    {
		free(results);
		free(operations);
		free(tempOp);
		free(operatorArray);
		fprintf(stdout,"\n Calculate results malloc failed.\n");
		return MALLOC_FAIL;
    }

	//call start function < parsing, evaluate turev and integral >
    Start(argv[1],operatorArray,numOfOperator,operations,
			numOfOperation,results,tempOp, atoi(argv[2]));
	 
    free(operatorArray);
    free(operations);
    free(results);
    free(tempOp);

    return 0;	
}



/***********************************************************************
 * Function name : int checkDoMathsArguments(int argc, CHARPTRPTR argv)
 * 
 * Description   :  doMaths için girilen argumanlari kontrol eder
 * 					arguman sayisini,
 * 					fonksiyon string'inin uzunlugunu
 * 					(kullanici bos string girerse hata)
 * 
 * 					time degerinin sayisal degerlerden olusup
 * 					olusmadigini kontrol eder
 *
 * 
 * Parameters    :  arguman sayisi(integer argc)
 * 					argumanlar string'i( char** argv)	   
 *
 * Return        :  integer
 * 					eger argumanlar dogru girildiyse SUCCESS return edilir,
 * 					diger durumlarda USAGE_ERROR  return edilir 
 * 
************************************************************************/
int checkDoMathsArguments(int argc, CHARPTRPTR argv)
{
	int i,k;
	
	CHARPTR s1 = "\n*************************************************\n";
	CHARPTR mes =
	   "\n USAGE : ./doMaths  \"function\"  time "
	   "\n\n There must be three arguments, respectively: "
	   "\n ./doMaths  ->  execution name"
	   "\n function   ->  your function  "
	   "\n time       ->  enter integer nanosecond for time "
	   "\n\n Please notice! Read Usage.. \n"
	   "\n***********************************************************\n";

	CLEAN_SCREEN;	

	/* arguman sayisi konrol edilir*/	
	if(argc != 3)
	{
		fprintf(stderr, "%s",s1);
		fprintf(stderr," \n You entered =>  ");		
		for(i=0; i<argc ; ++i)
		{	
			fprintf(stderr,"  %s ",argv[i]);
		}						
		fprintf(stderr,"\n\n Please enter three arguments!!!\n%s%s",s1,mes);

		return USAGE_ERROR;
	}

	/* time degeri kontrol edilir, integer*/
	/* yerine harf girilmisse hata return edilir*/
	for(k=0; k< strlen(argv[2]) ; ++k)
	{
		if( isNumber(argv[2][k]) == NOT_NUMBER)
		{
			fprintf(stderr, "%s",s1);
			fprintf(stderr," \n You entered =>  ");		
			for(i=0; i<argc ; ++i)
			{	
				fprintf(stderr,"  %s ",argv[i]);
			}						
			fprintf(stderr,"\n\n Please enter positive integer for time !!!\n%s%s",s1,mes);

			return USAGE_ERROR;
		}
	}


	

	return SUCCESS;
}





/***********************************************************************
 * Function name : char isOperator(char op)
 * 
 * Description   : Control of character is operator
 * 
 * Parameters    : a character		   
 *
 * Return        : int , return status if ch is operator, return IS_OPERATOR,
 * 				   otherwise return NOT_OPERATOR
 * 
************************************************************************/
int isOperator(char op)
{
	if(op == '*' || op == '+' || op == '-' || op == '/')
		return IS_OPERATOR;

	return NOT_OPERATOR;
}





/***********************************************************************
 * Function name : int isNumber(char ch)
 * 
 * Description   : Control of character is number?
 * 
 * Parameters    : a character		   
 *
 * Return        : int , return status if ch is number, return IS_NUMBER,
 * 				   otherwise return NOT_NUMBER
 * 
************************************************************************/
int isNumber(char ch)
{
	int i;
	char nums[] = "0123456789";

	for(i=0; i < strlen(nums) ; ++i)
	{
		if(ch == nums[i])
			return IS_NUMBER;
	}
	return NOT_NUMBER;
}


