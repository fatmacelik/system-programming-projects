/*#################################################################*/
/*
* SYSTEM PROGRAMMING - ODEV 4 - ADD THREAD INTO VIZE PROGRAM
*
* Author 		: FATMA CELIK 
* Student No    : 101044038    
*                                                  
* Date		    :  April 29, 2014/                                           
*                                                         
*/         
/*##################################################################*/


#include "types.h"


char fifo[SIZE]="";

/***********************************************************************
 * Function name : int isNumbers(char ch)
 * 
 * Description   : Control of character is number?
 * 
 * Parameters    : a character		   
 *
 * Return        : int , return status if ch is number, return IS_NUMBER,
 * 				   otherwise return NOT_NUMBER
 * 
************************************************************************/
int isNumbers(char ch)
{
	int i;
	char nums[] = "0123456789";

	for(i=0; i < strlen(nums) ; ++i)
	{
		if(ch == nums[i])
			return IS_NUMBER;
	}
	return NOT_NUMBER;
}



static inline int usage(int argc, char* argv[])
{
	int k,i;
	
	CHARPTR s1 = "\n*************************************************\n";
	CHARPTR mes =
	   "\n USAGE : ./integral  ServerPid  Time(ms) "
	   "\n\n There must be three arguments, respectively: "
	   "\n ./integral  ->  execution name"
	   "\n ServerPid   ->  Server pid "
	   "\n time        ->  enter integer milisecond for time "
	   "\n\n Please notice! Read Usage.. \n"
	   "\n***********************************************************\n";

	if(argc != 3)
	{
		fprintf(stderr, "%s",s1);
		fprintf(stderr," \n You entered =>  ");		
		for(i=0; i<argc ; ++i)
		{	
			fprintf(stderr,"  %s ",argv[i]);
		}						
		fprintf(stderr,"\n\n Please enter three arguments!!!\n%s%s",s1,mes);

		return USAGE_ERROR;
	}


	   
	/* pid degeri kontrol edilir, integer*/
	/* yerine harf girilmisse hata return edilir*/
	for(k=0; k< strlen(argv[1]) ; ++k)
	{
		if( isNumbers(argv[1][k]) == NOT_NUMBER)
		{
			fprintf(stderr, "%s",s1);
			fprintf(stderr," \n You entered =>  ");		
			for(i=0; i<argc ; ++i)
			{	
				fprintf(stderr,"  %s ",argv[i]);
			}						
			fprintf(stderr,"\n\n Please enter positive integer for pid !!!\n%s%s",s1,mes);

			return USAGE_ERROR;
		}
	}

	/* time degeri kontrol edilir, integer*/
	/* yerine harf girilmisse hata return edilir*/
	for(k=0; k< strlen(argv[2]) ; ++k)
	{
		if( isNumbers(argv[2][k]) == NOT_NUMBER)
		{
			fprintf(stderr, "%s",s1);
			fprintf(stderr," \n You entered =>  ");		
			for(i=0; i<argc ; ++i)
			{	
				fprintf(stderr,"  %s ",argv[i]);
			}						
			fprintf(stderr,"\n\n Please enter positive integer for time !!!\n%s%s",s1,mes);

			return USAGE_ERROR;
		}
	}

    
    return SUCCESS;
}








/* Signal Handler for specific signals
   Gets two parameters which are handler func and signal flag
   Return -1 on error , otherwise returns true */
int SetHandler( Handler hand , int sig )
{
    if( signal(sig, hand) == SIG_ERR )
        return ERROR;

    return SUCCESS;
}

/*
   Prints "I have lost" message on screen
   Ctrl + C Handler
*/
void InterruptHandlerChild(int sigNo)
{
    fprintf(stderr, "\nPid : %d ,I have lost.\n",getpid());
    unlink(fifo);
    exit(EXIT_FAILURE);
}




/***********************************************************************
 * Function name : int main(int argc, CHARPTRPTR argv)
 * 
 * Description   :  program test fonksiyonu
 * 
 * Parameters    :  arguman sayisi(integer argc)
 * 					argumanlar string'i( char** argv)	   
 *
 * Return        :  success, 0
 * 
************************************************************************/
int main(int argc, char* argv[])
{
    const char MAIN_FIFO[] = "./doMathFifo";
    char fifoName[SIZE]="integral";
    char logName[SIZE]="./integral";
    FILEPTR logFile;
    int fd=-1,fd2=-1;
    char name[SIZE] ="integral", connectTime[SIZE]="",temp[SIZE]="";
    int pid,time,myPid = getpid();
    CLEAN_SCREEN;
    double value=0,times=0;

     /* Handler for the parent. ctrl + c */
    if( SetHandler( InterruptHandlerChild , SIGINT) == ERROR )
    {
        fprintf(stderr,"\nFailed to set signal handlers.\n\n");
        return ERROR;
    }
    
	if( usage(argc,argv) != SUCCESS )
    {
        return EXIT_FAILURE;
    }
    printf("\n");



	sprintf(temp, "%d.fifo", myPid);
	strcat(fifoName,temp);
	strcpy(fifo,fifoName);
	sprintf(temp, "%d.log", myPid);
	strcat(logName,temp);
	
	logFile= fopen(logName,"w");
	
    if( mkfifo(fifoName, FIFO_PERMS) == ERROR)
	{
		fprintf(stdout,"\n Client : Failed to create <%s>fifo.\n\n",fifoName);
		return ERROR;
	}
	 
	if ((fd = open(MAIN_FIFO, O_WRONLY)) != ERROR)
	{
		pid = atoi(argv[1]);
		// Write pid

		if (write(fd, &pid, sizeof (int)) != ERROR)
		{
			fprintf(stdout, "\n Sent server pid: %d", pid);
		}
		
		if (write(fd, &myPid, sizeof (int)) != ERROR)
		{
			fprintf(stdout, "\n Sent my pid: %d", myPid);
		}

		time = atof(argv[2]);
		if (write(fd, &time, sizeof (int)) != ERROR)
		{
			fprintf(stdout, "\n Sent time : %d", time);
		}
		if (write(fd, name, SIZE) != ERROR)
		{
			fprintf(stdout, "\n Sent name : %s", name);
		}

		if (close(fd) == ERROR){
			fprintf(stdout,"\n Client : Failed to close main fifo.\n");
			return ERROR;
		}
	  
	}else{
		fprintf(stdout,"\n Client : Failed to open main fifo.\n");
		return ERROR;
	}        

    fprintf(stdout,"\n\n");
    
	
	
	// open and read childfifo		   
	if ((fd2 = open(fifoName, O_RDONLY)) != ERROR)
	{
		if (read(fd2,connectTime, SIZE)!= ERROR)
		{				
			fprintf(logFile, "%s", connectTime);
		}

		while (read(fd2,&value, sizeof (double))!= 0  && read(fd2,&times, sizeof (double))!= 0)
		{				
			fprintf(logFile, "%f%20s%f\n", value," ",times);
		}//while end
		
		if (close(fd2) == ERROR){
			fprintf(stdout,"\n Client : Failed to close <%s> fifo.\n",fifoName);
			unlink(fifoName);
			return 0;
		}
		unlink(fifoName);
	}
	else
	{
		fprintf(stdout,"\n Client : Failed to open integral fifo.\n");	
		return ERROR;
	}	
				
				
    
	fclose(logFile);
	
    return 0;
}
