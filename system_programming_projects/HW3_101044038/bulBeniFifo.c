/*######################################################################
 * 
 * LECTURE NAME  :   BIL244 SYSTEM PROGRAMMING /  2014
 * 
 * HOMEWORK  2   :   ./bulBeni dirName -g "string" -l numOfLine
 * 
 * STUDENT       :   FATMA CELIK - 101044038
 *
 * DATE          :   18 MARCH 2014 / TUESDAY
 * 
######################################################################*/

#include "bulBeniFifo.h"

/* save arrays to free because of control c*/
CHARPTRPTR resourceChPtPt[RESOURCE_CHPTRPTR] = {NULL};
INTPTR resourceIntPtr[RESOURCE_INTPTR] = {NULL};



pid_t childs[MAX_CHILD];
pid_t parentPID;
pid_t childPID;


static int numbersFile;
/***********************************************************************
 * Function name : void  myHandler(int sig)
 * 
 * Description   : control c handler
 * 
 * Parameters    : int sig
 *
 * Return        : void
 * 
************************************************************************/

int setHandler( Handler hand , int sig )
{
    if( signal(sig, hand) == SIG_ERR )
        return HANDLER_ERROR;

    return SUCCESS;
}

void  myHandler(int signum)
{
	int i;
	pid_t temp;

	if(resourceChPtPt[0] != NULL)
		Free(resourceChPtPt[0],(int)(sizeof(resourceChPtPt[0])));

	if(resourceChPtPt[1] != NULL)
		Free(resourceChPtPt[1],(int)(sizeof(resourceChPtPt[1])));
		
	if(resourceIntPtr[0] != NULL)
		free(resourceIntPtr[0]);

	temp = getpid();
	if(parentPID == temp)
	{
			for(i=0; i < numbersFile ;++i)
				kill(childs[i],SIGINT);

			fprintf(stdout,"\n PARENT PID : %d  Parent died. \n",parentPID);
			fprintf(stdout,"\n Control C handled..\n Program is finished..."
					"\n Good Days.. \n Fatma Celik\n"
					"\n ------------------------------\n\n");
	}
	else
	{
			fprintf(stdout,"\n\n -----------------------------"
			               "\n CHILD PID  : %d  Childs died.  \n",temp);
			
	}

	exit(EXIT_SUCCESS);
}







/***********************************************************************
 * Function name : int doChildParentOperation(CHARPTRPTR filesNames,
 * 						int numOfFile,CHARPTR target,int numOfLine)
 * 
 * Description   : operations of child and parent
 * 
 * Parameters    : file names, number of files, search target, number of line
 *
 * Return        : int
 * 
************************************************************************/
int doChildParentOperation(CHARPTRPTR filesNames,int numOfFile,CHARPTR target,int numOfLine)
{
	int i=0,status;
	pid_t pID,parent;

	numbersFile=numOfFile;
	parent=getpid();
	parentPID = parent;
	fprintf(stdout,"\b Parent PID: %d \n\n",parent);
	
	for(i=0; i<numOfFile; ++i)
	{
	
		if( (pID = fork() ) == 0 )
		{
				childPID=getpid();
				childs[i]=childPID;
				
				status = childOperation(filesNames[i],target,numOfLine,i+1);
				if(status == EXIT)
				{
					kill(pID, SIGINT);
					break;

					return EXIT;
				}
				break;
		}

		else if(pID < 0)
		{
			fprintf(stderr,"\n Fork failed. Program terminated..\n\n");
			return EXIT;			
		}else if( pID > 0)
		{
			while( Wait(NULL) > 0 );
		}	

		
	}/*end for*/

	if(parent == getpid())
	{
		fprintf(stdout,"\n\n Program has finished writing.. \n Good Days.. \n Fatma Celik \n\n");
		return EXIT;

	}

		
	return CONTINUE;
}





/***********************************************************************
 * Function name : childOperation(CHARPTR filesNames,CHARPTR target,
 * 						int numOfLine, int childNum)
 * 
 * Description   : operations of child and parent
 * 
 * Parameters    : file names, number of files, search target, number of line
 *
 * Return        : int
 * 
************************************************************************/
int childOperation(CHARPTR filesNames,CHARPTR  target,int numOfLine, int childNum)
{
	
	int status;
	FILEPTR inputFile;
	int numLinesIntoFile=0;
	INTPTR sizeOfLine;/* number of characters in a line in file*/
    const char fifoName[] = "./mainFifo";
    char childFifo[PID_LEN] = {0};
	sprintf(childFifo, "./%d", childNum);

	
	inputFile = fopen(filesNames,"r");
	
	if( inputFile != NULL)
	{

		/* number of lines in file*/
		numLinesIntoFile = FindNumberOfLine(filesNames);

		if( numLinesIntoFile > 0)
		{
			/* malloc for read datas from file*/
			sizeOfLine = (int*)malloc(sizeof(int) * numLinesIntoFile);
			resourceIntPtr[0]=sizeOfLine;
			if(sizeOfLine != NULL)
			{
				/*find number of characters in file and */

				/*save numer of chars in integer array sizeOfLine*/	
				FindNumOfChars(filesNames,sizeOfLine);

				
				status =GrepM(filesNames,sizeOfLine,numLinesIntoFile,target, fifoName);

			//status = startPrint(fifoName,numOfLine);																

				if(status == EXIT)
				{
					if(resourceChPtPt[0] != NULL)
						Free(resourceChPtPt[0],(int)(sizeof(resourceChPtPt[0])));

					if(resourceChPtPt[1] != NULL)
						Free(resourceChPtPt[1],(int)(sizeof(resourceChPtPt[1])));
						
					if(resourceIntPtr[0] != NULL)
						free(resourceIntPtr[0]);

					fclose(inputFile);
					
					return EXIT;
					
				}
				
				
				free(sizeOfLine);		/*call free for malloc*/					
			}
			else
			{
				fprintf(stdout,"\n Malloc is failed in dochildparentoperation function..\n");
				free(sizeOfLine);		/*call free for malloc*/
				fclose(inputFile);
				exit(ERROR);
			}

		}
		
	}
	else
	{
		fprintf(stdout, "\n This file name: %s does not opened.",filesNames);
	}

	fclose(inputFile);

	
	return CONTINUE;
}















/***********************************************************************
 * Function name : void Start(CHARPTRPTR argv)
 * 
 * Description   : program starts and all other functions in this function
 * 
 * Parameters    : arguments, char** argv	   
 *
 * Return        : int
 * 
************************************************************************/
int Start(CHARPTRPTR argv)
{
	int numOfDir = 0,		/* number of directory*/
		numOfFile = 0,		/* number of file*/
		i=0; 				/* index*/
	int index = 0,status;
		
	CHARPTRPTR filesNames={NULL};  /* contains  all files names */
	
	/* call function to find  number of directory and files */
	FindNumOfFileDir(argv[1], &numOfFile, &numOfDir);

	filesNames = (CHARPTRPTR)malloc(sizeof(CHARPTR)* numOfFile);
	resourceChPtPt[0] = filesNames; /* signal handler to free*/
	if(filesNames == NULL)
	{
		fprintf(stderr,"\n Malloc is failed: <filesNames array> "
						"\n Program is terminated...\n\n ");
		free(filesNames);
		exit(ERROR);
	}/*if end*/
	else
	{
		for(i=0; i<numOfFile;++i)
		{
			filesNames[i] = (CHARPTR)malloc(sizeof(char)*( strlen(argv[1])*2 ));
			if(filesNames[i] == NULL)
			{
				fprintf(stderr,"\n Malloc is failed: <filesNames array> "
								"\n Program is terminated...\n\n ");
				Free(filesNames,i+1);
				exit(ERROR);
			}/*if end*/
	
		}/*for end*/

	}/*else end*/

	/*call function to save files names in array*/
	saveFilesNames(argv[1],filesNames,&index);
	
	fprintf(stdout,"\n --------------------------------------\n");
	fprintf(stdout,"\n Program has found %d files in \"%s\" directory.\n",numOfFile,argv[1]);
	fprintf(stdout,"\n --------------------------------------\n");

	status = doChildParentOperation(filesNames,numOfFile,argv[3],atoi(argv[5]));

	if(status == EXIT)
	{
		fprintf(stdout,"\n");
		Free(filesNames,numOfFile);
		return EXIT;
	}
	
	Free(filesNames,numOfFile);

	return CONTINUE;
}


/***********************************************************************
 * Function name : char getCh(void)
 * 
 * Description   : take a character from terminal
 * 
 * Parameters    : void
 *
 * Return        : a character from terminal
 * 
************************************************************************/ 
char getCh(void)
 {
    struct termios oldt, newt;    
    int ch;
    /* Initialize new terminal i/o settings */
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    
    /* get character */
    ch = getchar();
    
    /* Restore old terminal i/o settings */
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    
    return (char)ch;
}
/***********************************************************************
 * Function name : GrepM(CHARPTR fileName, INTPTR sizeOfLine, int numOfLine,
 * 							CHARPTR target, FILEPTR outPtr)
 * 
 * Description   : Search target in file and save number of target in file 
 * 
 * Parameters    : input file name,
 * 				   number of characters in line(size of line),
 * 				   number of line in file,
 * 				   search string target			   
 *
 * Return        : void
 * 
************************************************************************/
int GrepM(CHARPTR fileName, INTPTR sizeOfLine, int numOfLine, CHARPTR target, const char fifoName[])
{
	FILEPTR fPtr;
	CHARPTRPTR str;
	int i,count=0,sum=0;
	char childFifo[PID_LEN] = {0};
	char tempCh;
	int fd;
    int flag;
    
	fPtr = fopen(fileName,"r");

    if ( fPtr== NULL )
    {
		fprintf(stdout,"\n Not open file. (Function name : GrepM)\n");
		fclose(fPtr); /* close file*/		
		exit(ERROR);
	}
    else
    {
		/* malloc for  2d char array*/
		str = (char**)malloc(sizeof(char*) * numOfLine);
		resourceChPtPt[1]=str;
		if( str != NULL)
		{
					sprintf(childFifo, "./%d", childPID);

				 if ((fd = open(childFifo, O_WRONLY)) != ERROR)
				{
								for(i=0; i<numOfLine;++i)
								{
									/*malloc to read string  for i. line*/
									str[i] = (char*)malloc(sizeof(char) * ( sizeOfLine[i] * 2) );
									if(str[i] != NULL )
									{
										/*read string  i. line */
										fgets(str[i], sizeof(char*) * (sizeOfLine[i] * 2), fPtr);
											
										/* count is number of target in str[i]*/
										/*search target in string str[i]*/
										count = Search(str[i],target);
										if(count !=0)
										{

											if (write(fd, &childPID, sizeof(int)) != ERROR)
											{
												//fprintf();
											}

											if (read(fd, &childPID, sizeof (int)) != ERROR)
											{
												//fprintf(stderr, "\n PID : %d\n", pid);
											}


											fprintf(stdout,"\n PID : %d  File Name: %s  Line:%3d"
											"  Number of  \"%s\": %d ",childPID,fileName,i,target,count);
																									   
										}
										sum += count;/* sum of <number of target>*/
										count=0;					
										
									}/* end if str != null*/
									else
									{
										fprintf(stdout,"\n Malloc is failed in GrepM function.(str[%d])!! ",i);
										Free(str,i+1);
										fclose(fPtr); /* close file*/		
										exit(ERROR);
									}
								}/*end for*/

					if (close(fd) == ERROR){
									perror("\n Failed to close main fifo.\n");
								}			
			flag=0;
			do{           
					tempCh = getCh();                
					if(tempCh == '\n')
					{                  
					   return CONTINUE;           
					}
					else if( tempCh == 'q')
					{
						flag = 0;
						fprintf(stdout,"\n\n User entered \"q\"..\n Exiting from program.. "
								"\n Good days.. \n Fatma Çelik \n\n ");
						return EXIT;
					}
					else
						flag = 1;         
            
        }while(flag);




								
				}else{
							perror("\n Failed to open main fifo.\n");
						}  

			

		}/*end if*/
		else
		{
			fprintf(stdout,"\n Malloc is failed in GrepM function.(str)!! ");
			free(str);
			fclose(fPtr); /* close file*/			
			exit(ERROR);
		}/*end else*/ 
		
		fclose(fPtr); /* close file*/
		Free(str,numOfLine);/* call free function for malloc*/
		
	}/*end else*/


return CONTINUE;
}/*end function*/




