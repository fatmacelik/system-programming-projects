/*######################################################################
 * 
 * LECTURE NAME  :   BIL244 SYSTEM PROGRAMMING /  2014
 * 
 * HOMEWORK  3   :   ./bulBeniPipe dirName -g "string" -l numOfLine
 * 
 * STUDENT       :   FATMA CELIK - 101044038
 *
 * DATE          :   3 april / thursday
 * 
######################################################################*/

#ifndef BULBENIFIFO_H
#define BULBENIFIFO_H

	/************************** INCLUDES *********************************/
	#include <stdio.h>   /* printf()           */
	#include <string.h>  /* strcmp(), strlen() */
	#include <stdlib.h>  /* malloc()           */
	#include <unistd.h>
	#include <errno.h>
	#include <dirent.h>
	#include <wait.h>
	#include <ctype.h>    
	#include <termios.h>
	#include <sys/types.h>
	#include <signal.h>
	#include <sys/stat.h>
	#include <fcntl.h>
	/************************** DEFINES **********************************/

	#define HANDLER_ERROR -1
	#define ERROR 0
	#define NOT_OPEN -1
	#define SUCCESS 1
	
	#define CLEAR_SCREEN system("clear")
		
	#define EXIT 0
	#define CONTINUE 1


	#define FAIL -1
	#define NOT_NUMBER -1

	#define NOT_EOF ( (ch) != EOF )  

	#define RESOURCE_CHPTRPTR 2 //two
	#define RESOURCE_INTPTR 1 //one

	#define FIFO_PERMS (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

	#define PID_LEN 10
	#define MAX_CHILD 10000
	/************************** TYPEDEFS *********************************/
	typedef int*   INTPTR; 		 /* integer pointer       */
	typedef char*  CHARPTR;      /* char pointer          */
	typedef char** CHARPTRPTR;   /* two dimensional array */
	typedef FILE*  FILEPTR;      /* FILE pointer          */
	typedef DIR*   DIRPTR;       /* DIRECTORY  pointer    */
	typedef void (*Handler)(int);/* Signal handler */


	int setHandler( Handler hand , int sig );
	void  myHandler(int signum);

	int checkDirName(CHARPTR name);
	
	void FindNumOfFileDir(CHARPTR dirName,INTPTR numOfFile, INTPTR numOfDir);
	void saveFilesNames(CHARPTR dirName,CHARPTRPTR filesNames,INTPTR i);
	int doChildParentOperation(CHARPTRPTR filesNames,int numOfFile,CHARPTR target,int numOfLine);
	int childOperation(CHARPTR filesNames,CHARPTR  target,int numOfLine, int childNum);


	/************************** DIRECTORY FUNCTIONS ********************************/
	
	/* check of main arguments*/
	int Usage(int argc, CHARPTRPTR argv);
	void printMes(int argc, CHARPTRPTR argv);
	/* check of number characters*/
	int isNumber(char ch);
	/* program starts and call all other functions in this function*/
	int Start(CHARPTRPTR argv);
	
	/* check is directory */
	int isdir( struct dirent *direntp );
	/* close directory*/
	void closeDir(DIRPTR dirPtr);
	/* set directory path name */
	void pathName(CHARPTR path ,CHARPTR dirName,CHARPTR name );	

	/*check path name*/
	int CheckPath(CHARPTR path);
	
	/************************** GREP FUNCTIONS ********************************/
	
	/* start to find target in files*/
	void StartGrep(CHARPTR fileName, CHARPTR target, FILEPTR outPtr);
	
	/* find number of lines in input file*/
	int  FindNumberOfLine(CHARPTR fileName);
	
	/*  check of target is sub string of str*/
	int  IsSubStr(CHARPTR str, CHARPTR target);
	
	/* search target in string str*/
	int  Search(CHARPTR str, CHARPTR target);
		
	/* find number of characters in a line in input file*/
	void FindNumOfChars(CHARPTR fileName, INTPTR  lines);
	
	/* grep function, call search function and search target in file*/
	int GrepM(CHARPTR fileName, INTPTR sizeOfLine, int numOfLine, CHARPTR target, const char fifoName[]);
	
	/* deallocate - free function for two dimensional array*/
	void Free(CHARPTRPTR str, int size);
	/*create fork*/
	pid_t Fork();
	pid_t Wait(int *stat_loc);

	/* take a command from terminal*/
	char getCh(void) ;
	
#endif

