/*######################################################################
 * 
 * LECTURE NAME  :   BIL244 SYSTEM PROGRAMMING /  2014
 * 
 * HOMEWORK  3   :   ./bulBeniPipe dirName -g "string" -l numOfLine
 * 
 * STUDENT       :   FATMA CELIK - 101044038
 *
 * DATE          :   3 april / thursday
 * 
######################################################################*/

#include "bulBeniFifo.h"



/***********************************************************************
 * Function name : void saveFilesNames(CHARPTR dirName,CHARPTRPTR filesNames,INTPTR i)
 * 
 * Description   : save files names into filesNames array,index
 * 
 * Parameters    : directory name, filesNames array
 * 				   
 * Return        : void
************************************************************************/
void saveFilesNames(CHARPTR dirName,CHARPTRPTR filesNames,INTPTR i)
{
	DIRPTR dirPtr; 			/* directory object from opendir() 			*/
    struct dirent *direntp; /* information about next directory entry   */
    CHARPTR path={NULL}; /* temporary char arrya for directory name  */
	
	/* open the directroy */   
	dirPtr = opendir(dirName);		
	/* opendir() status checked */
	if(dirPtr != NULL)
	{    		
		while( (direntp = readdir(dirPtr)) != NULL)
		{			
			path = (CHARPTR)malloc(sizeof(char) * (strlen(dirName) + strlen(direntp->d_name)) * 2 );
			if(path == NULL)
			{
				fprintf(stderr,"\n Malloc is failed.<path>\n\n");
				free(path);
				Free(filesNames,(int)(sizeof(filesNames)));
				closeDir(dirPtr);
				exit(ERROR);
			}			
			
			path[0] = '\0';
			pathName(path,dirName,direntp->d_name);/*seth directory path name*/
            
			if( checkDirName(direntp->d_name))
			{					
				if(!isdir(direntp) )
				{
					if( CheckPath(path)  == SUCCESS)
					{
						free(filesNames[*i]);
						filesNames[*i] = (CHARPTR)malloc(sizeof(char)*(strlen(path) * 1.5) );
						if(filesNames[*i] == NULL)
						{
							fprintf(stderr,"\n Malloc is failed.<filesNames[*%d]>\n\n",*i);
							free(path);
							Free(filesNames,(int)(sizeof(filesNames)));
							closeDir(dirPtr);
							exit(ERROR);
						}

						filesNames[*i][0]='\0';
						strcat(filesNames[*i],path);
						++(*i);						
					}
				}
				else
				{					
					saveFilesNames(path,filesNames,i);						
				}					
			}			
			/*free path*/
			free(path);				
		}/*while end */
		
       closeDir(dirPtr); /* close directory */
	}/* if end */
}




/***********************************************************************
 * Function name : void FindNumOfFileDir(CHARPTR dirName,
 * 					INTPTR numOfFile, INTPTR numOfDir)
 * 
 * Description   : find number of files&directories in dirName directory
 * 
 * Parameters    : directory name, number of directory pointer,
 *                  number of files
 * 				   
 *
 * Return        : void
************************************************************************/
void FindNumOfFileDir(CHARPTR dirName,INTPTR numOfFile, INTPTR numOfDir)
{
    DIRPTR dirPtr; 			/* directory object from opendir() 			*/
    struct dirent *direntp; /* information about next directory entry   */
    CHARPTR path={NULL}; /* temporary char arrya for directory name  */ 	
		
	/* open the directroy */   
	dirPtr = opendir(dirName);

	/* opendir() status checked */
	if(dirPtr != NULL)
	{    		
		while( (direntp = readdir(dirPtr)) != NULL)
		{
			path = (CHARPTR)malloc(sizeof(char) * (strlen(dirName) + strlen(direntp->d_name)) * 2 );
			if(path == NULL)
			{
				fprintf(stderr,"\n Malloc is failed.<path>\n\n");
				free(path);
				closeDir(dirPtr);
				exit(ERROR);
			}					
			path[0] = '\0';
			pathName(path,dirName,direntp->d_name);/*seth directory path name*/
            
			if( checkDirName(direntp->d_name) )
			{					
				if(!isdir(direntp) )
				{
					if( CheckPath(path)  == SUCCESS)
					{	
						++(*numOfFile);
					}
				}
				else{
					++(*numOfDir);												
					FindNumOfFileDir(path,numOfFile,numOfDir);					
				}					
			}			

				free(path);
		}/*while end */
		
       closeDir(dirPtr); /* close directory */
	}/* if end */

}

/***********************************************************************
 * Function name : Search(CHARPTR str, CHARPTR target )
 * 
 * Description   : Search target and find number of target in str
 * 
 * Parameters    : two char array str and target			   
 *
 * Return        : int , return number of target in str
 * 
************************************************************************/
int Search(CHARPTR str, CHARPTR target )
{
	int count=0, status=0;
	int i;

	for(i=0; i< strlen(str) ; ++i)
	{
		status = 0;
		if( str[i] == target[0]) 
		{/* if target first character is found, call IsSubStr function*/
			status = IsSubStr(&str[i], target);

			if(status == 1)
				count++; /* if target is found, increase count*/
		}
	}

	return count;
}

/***********************************************************************
 * Function name : IsSubStr(CHARPTR str, CHARPTR target)
 * 
 * Description   : Control of target is sub string in str
 * 
 * Parameters    : two char array str and target			   
 *
 * Return        : int , return status if target is in str, return 1,
 * 				   otherwise return 0
 * 
************************************************************************/
int IsSubStr(CHARPTR str, CHARPTR target)
{
	int i;
	
	for( i=0; i<strlen(target); ++i)
	{
		if(target[i] != str[i])
			return 0;   /* letters not same, then target */
					    /*is not sub string, return false*/
	}


	return 1; /* if target is sub string of str, return true*/
}

/***********************************************************************
 * Function name : Free(CHARPTRPTR str ,int size)
 * 
 * Description   : free function for malloc 
 * 
 * Parameters    : 2D char array and size			   
 *
 * Return        : void
 * 
************************************************************************/
void Free(CHARPTRPTR str ,int size)
{
	int i;

	for(i=0; i< size; i++)
	{
		if(str[i] != NULL)
			free(str[i]);
	}

	if(str != NULL)
		free(str);
}







/***********************************************************************
 * Function name : FindNumOfChars(CHARPTR filename,INTPTR lines)
 * 
 * Description   : find number of characters in a line
 * 
 * Parameters    : input file name, integer array lines	   
 *
 * Return        : void
 * 
************************************************************************/
void FindNumOfChars(CHARPTR fileName,INTPTR lines)
{
    FILEPTR fPtr;
    char c;
    int line = 0,count=0;

	fPtr = fopen(fileName, "r");

	
    if(fPtr  == NULL)
    {
		fprintf(stdout,"\n Not open file in FindNumOfChars fnction.\n");
		fclose(fPtr);/*close file*/
		exit(ERROR);
    }
    else
    {       
		do{
			c = fgetc(fPtr);
			count++;

			if(c == '\n')
			{
				/* save number of characters in a line*/
				lines[line]=count;
				line++;/* increase line index*/
				count=0;
			}
								
		}while(c != EOF);
		
		fclose(fPtr);/*close file*/
    }	 
}



/***********************************************************************
 * Function name : FindNumberOfLine(CHARPTR fileName)
 * 
 * Description   : find number of line in input file
 * 
 * Parameters    : input file name	   
 *
 * Return        : int, return number of lines in file
 * 
************************************************************************/
int FindNumberOfLine(CHARPTR fileName)
{
    FILEPTR fPtr;
    char c = '0';
    int line = 0,count=0;

	fPtr = fopen(fileName, "r");	

    if(fPtr == NULL)
    {		
        fprintf(stdout,"\n File not open in FindNumberOfLine function. \n");
        fclose(fPtr); /* close file */
		exit(ERROR);
    }
    else
    {       
		do{
			c = fgetc(fPtr); /* read character in file*/

			if(c == '\n')
				count++; /* increase line number*/
				
		}while(c != EOF);

		line = count; /* number of line in file*/

        fclose(fPtr); /* close file */
    }

    return line;
}




/***********************************************************************
 * Function name : int CheckPath(CHARPTR path)
 * 
 * Description   : Control of path name
 * 
 * Parameters    : char pointer path name		   
 *
 * Return        : int, return status if there is '~' symbol in path name,
 * 					return 0,
 * 				   otherwise return 1
 * 
************************************************************************/
int CheckPath(CHARPTR path)
{
	int i=0;

	for(i=0; i< strlen(path) ; ++i)
		if(path[i] == '~')
			return ERROR;
			
	return SUCCESS;
}









/***********************************************************************
 * Function name : int isNumber(char ch)
 * 
 * Description   : Control of character is number?
 * 
 * Parameters    : a character		   
 *
 * Return        : int , return status if ch is number, return 1,
 * 				   otherwise return 0
 * 
************************************************************************/
int isNumber(char ch)
{
	int i;
	char nums[] = "0123456789";

	for(i=0; i < strlen(nums) ; ++i)
	{
		if(ch == nums[i])
			return SUCCESS;
	}
	return ERROR;
}





/***********************************************************************
 * Function name : pid_t Wait(int *stat_loc)
 * 
 * Description   : 
 * 
 * Parameters    : 		   
 *
 * Return        : 
 * 
************************************************************************/
pid_t Wait(int *stat_loc)
{
    int retVal;

    while( (retVal = wait(stat_loc)) == -1 && (errno == EINTR) );

    return retVal;
}


/***********************************************************************
 * Function name : pid_t Fork()
 * 
 * Description   : create fork
 * 
 * Parameters    : there is not any parameters	   
 *
 * Return        : return pid
 * 
************************************************************************/
pid_t Fork()
{
     pid_t pid;
     if( ( pid = fork() ) == -1 )
     {
        perror("Failed to fork :");
        exit(-1);
     }
     return pid;
}


/***********************************************************************
 * Function name : void closeDir(DIRPTR dirPtr)
 * 
 * Description   : close directory
 * 
 * Parameters    : directory pointer
 *
 * Return        : void
 * 
************************************************************************/
void closeDir(DIRPTR dirPtr)
{
    while ((closedir(dirPtr) == -1) && (errno == EINTR)) ;
}

/***********************************************************************
 * Function name : void pathName(CHARPTR path ,CHARPTR dirName,CHARPTR name)
 * 
 * Description   : set directory path name
 * 
 * Parameters    : char pointer, pathname,
 * 				   directory name and direntp->d_name
 *
 * Return        : void
 * 
************************************************************************/
void pathName(CHARPTR path ,CHARPTR dirName,CHARPTR name)
{
	strcpy(path,dirName);
	strcat(path,"/");
	strcat(path,name);   
}



/***********************************************************************
 * Function name : int isdir( struct dirent *direntp )
 * 
 * Description   : check directory
 * 
 * Parameters    : struct dirent *direntp
 *
 * Return        : integer status, if is directory return true,
 * 				                   else false
************************************************************************/
int isdir( struct dirent *direntp )
{
    return direntp->d_type == DT_DIR ;          
}






/***********************************************************************
 * Function name : int checkDirName(CHARPTR name)
 * 
 * Description   : check directory name
 * 
 * Parameters    : directory name
 *
 * Return        : integer status,if directory is "." and "." return true
 * 									else return false 
************************************************************************/
int checkDirName(CHARPTR name)
{
    return strcmp("." , name) && strcmp("..", name);
}


