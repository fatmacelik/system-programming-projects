/*######################################################################
 * 
 * LECTURE NAME  :   BIL244 SYSTEM PROGRAMMING /  2014
 * 
 * HOMEWORK  3   :   ./bulBeniPipe dirName -g "string" -l numOfLine
 * 
 * STUDENT       :   FATMA CELIK - 101044038
 *
 * DATE          :   3 april / thursday
 * 
######################################################################*/

#include "bulBeniFifo.h"

/***********************************************************************
 * Function name : main(int argc, CHARPTRPTR argv)
 * 
 * Description   : calls Start() function to run program
 * 
 * Parameters    : (integer) argc is number of arguments ,
 * 				   (two dimensional character array) argv holds arguments
 *
 * Return        :  return 0
 * 
************************************************************************/
int main(int argc, CHARPTRPTR argv)
{
	/* control c handler*/
	if( setHandler( myHandler , SIGINT) == HANDLER_ERROR)
	{
		fprintf(stderr,"\n Signal handler not succesfull..\n\n");
		return ERROR;
	}
	/*----------------------------------------*/
	
	/*  checked arguments */
	if( Usage(argc,argv)  == SUCCESS)
	{
		return Start(argv);
			
	}

    return 0;
}






/***********************************************************************
 * Function name : Usage(int argc, CHARPTRPTR argv)
 * 
 * Description   : Check of arguments to entered from terminal
 * 
 * Parameters    : (integer) argc is number of arguments ,
 * 				   (two dimensional character array) argv holds arguments
 *
 * Return        : If arguments were entered correct, return SUCCESS(1)
 * 				   otherwise return ERROR(0)
 * 
************************************************************************/
int Usage(int argc, CHARPTRPTR argv)
{
	int i=0;
	DIRPTR dirPtr;
	CHARPTR s1 = "\n*****************************************"
				 "************************************\n";
	CHARPTR mes =
	   "\n USAGE : ./bulBeniFifo  dirName -g  \"string\"  -l numOfLine "
	   "\n\n There must be six arguments, respectively: "
	   "\n ./bulBeniFifo  ->  execution name"
	   "\n dirName        ->  directory name  "
	   "\n -g             ->  remember to grep and be '-' symbol in front of g"
	   "\n \"string\"       ->  search string  between two (\") symbol "
	   "\n -l             ->  list of lines  "
	   "\n numOfLine      ->  enter a number for list to found lines of string"
	   "\n\n Please notice! Read Usage.. \n"
	   "\n*****************************************"
				 "************************************\n";

	/*---------------------------------------------------------------*/
	CLEAR_SCREEN;
	   
	/*---------------------------------------------------------------*/
	/* check number of arguments*/
	if( argc != 6)
	{
		printMes(argc,argv);
		fprintf(stdout,"\n Please enter six arguments!!\n%s%s",s1,mes);
		return ERROR;
	}
	/*---------------------------------------------------------------*/
	/* check execution name */
	if(strcmp(argv[0],"./bulBeniFifo") != 0)
	{
		printMes(argc,argv);
		fprintf(stdout,"\n You entered incorrect entry. "
					   "\n Please check execution name!!\n%s%s",s1,mes);
		return ERROR;			   
	}
	
	/*---------------------------------------------------------------*/
	/* check -g */
	if(strcmp(argv[2],"-g") != 0)
	{
		printMes(argc,argv);
		fprintf(stdout,"\n You entered incorrect entry. "
					   "\n Please check arguments \" %s \"""!!\n%s%s",argv[2],s1,mes);
		return ERROR;			   
	}
	/*---------------------------------------------------------------*/
	/* check -l */
	if(strcmp(argv[4],"-l") != 0)
	{
		printMes(argc,argv);
		fprintf(stdout,"\n You entered incorrect entry. "
					   "\n Please check arguments \" %s \"""!!\n%s%s",argv[4],s1,mes);
		return ERROR;			   
	}
	/*---------------------------------------------------------------*/
	/* check number of lines */

	for(i=0; i<strlen(argv[5]);++i)
	{
		if( !isNumber(argv[5][i]) )
		{			
			fprintf(stdout,"\n You entered incorrect entry. "
			"\n Please check number of lines. \n%s%s",s1,mes);

			return ERROR;
		}
	}

	/*---------------------------------------------------------------*/

	/* open the directroy */   
	dirPtr = opendir(argv[1]);
	if(dirPtr == NULL)
	{
		printMes(argc,argv);
		fprintf(stdout,"\n You entered incorrect entry. "
			"\n %s : Directory not opened.  "
			"\n Please check directory name. \n%s%s",argv[1],s1,mes);

		closeDir(dirPtr);
		return ERROR;	
	}
	
	closeDir(dirPtr);
		
	return SUCCESS;
}

/***********************************************************************
 * Function name : void printMes(int argc, CHARPTRPTR argv)
 * 
 * Description   : print message on screen
 * 
 * Parameters    : (integer) argc is number of arguments ,
 * 				   (two dimensional character array) argv holds arguments
 *
 * Return        : void
 * 
************************************************************************/
void printMes(int argc, CHARPTRPTR argv)
{
	int i;
	CHARPTR s1 = "\n*****************************************"
				 "************************************\n";
	fprintf(stdout,"%s\n You entered ->",s1);
	for(i=0; i<argc;++i)
		fprintf(stdout," %s ",argv[i]);
	fprintf(stdout,"\n");

}
