/*######################################################################
 * 
 * LECTURE NAME  :   BIL244 SYSTEM PROGRAMMING /  2014
 * 
 * HOMEWORK  2   :   ./bulBeni dirName -g "string" -l numOfLine
 * 
 * STUDENT       :   FATMA CELIK - 101044038
 *
 * DATE          :   18 MARCH 2014 / TUESDAY
 * 
######################################################################*/

#include "bulBeni.h"



/***********************************************************************
 * Function name : void Start(CHARPTRPTR argv)
 * 
 * Description   : program starts and all other functions in this function
 * 
 * Parameters    : arguments, char** argv	   
 *
 * Return        : void
 * 
************************************************************************/
void Start(CHARPTRPTR argv)
{
	int numOfDir = 0, numOfFile = 0;
	FILEPTR dirFilePtr,filePtr,outPtr; 			/* write directories name in file  			*/
	pid_t pid;
	FILEPTR resultPtr;


	resultPtr = fopen(OUTPUT,"r");		
	dirFilePtr  = fopen(DIRECTORIES,"w");
	outPtr  = fopen(OUTPUT,"w");
	if(dirFilePtr != NULL  && outPtr != NULL)
	{
		filePtr = fopen(FILES,"w");
		if(filePtr != NULL)
		{
			/* copies directory names into char array 'path' */

			if((pid = fork()) == 0)
				FindNumOfDir(argv[1],&numOfDir,dirFilePtr,&numOfFile,filePtr,argv[3],outPtr);

			else if(pid > 0)
				while( Wait(NULL) > 0 );

			startPrint(resultPtr,atoi(argv[5]));
		
			/* close files */
			fclose(dirFilePtr);
			fclose(filePtr);
			fclose(outPtr);
			fclose(resultPtr);
	
		}/*if end*/
		else /* open file error message*/
		{
			fclose(dirFilePtr);
			fclose(filePtr);
			fclose(outPtr);
			fclose(resultPtr);
			fprintf(stdout,"\n Program terminated. \"%s\" input file is not opened...\n\n",FILES);
		}
				
	}/*if end*/
	else /* open file error message*/
	{
		fclose(dirFilePtr);
		fclose(outPtr);
		fclose(resultPtr);
		fprintf(stdout,"\n Program terminated. \"%s\" input file is not opened...\n\n",DIRECTORIES);
		
	}
}



/***********************************************************************
 * Function name : void closeDir(DIRPTR dirPtr)
 * 
 * Description   : close directory
 * 
 * Parameters    : directory pointer
 *
 * Return        : void
 * 
************************************************************************/
void closeDir(DIRPTR dirPtr)
{
    while ((closedir(dirPtr) == -1) && (errno == EINTR)) ;
}

/***********************************************************************
 * Function name : void pathName(CHARPTR path ,CHARPTR dirName,CHARPTR name)
 * 
 * Description   : set directory path name
 * 
 * Parameters    : char pointer, pathname,
 * 				   directory name and direntp->d_name
 *
 * Return        : void
 * 
************************************************************************/
void pathName(CHARPTR path ,CHARPTR dirName,CHARPTR name)
{
	strcpy(path,dirName);
	strcat(path,"/");
	strcat(path,name);   
}



/***********************************************************************
 * Function name : int isdir( struct dirent *direntp )
 * 
 * Description   : check directory
 * 
 * Parameters    : struct dirent *direntp
 *
 * Return        : integer status, if is directory return true,
 * 				                   else false
************************************************************************/
int isdir( struct dirent *direntp )
{
    return direntp->d_type == DT_DIR ;          
}



/***********************************************************************
 * Function name : int checkDirName(CHARPTR name)
 * 
 * Description   : check directory name
 * 
 * Parameters    : directory name
 *
 * Return        : integer status,if directory is "." and "." return true
 * 									else return false 
************************************************************************/
int checkDirName(CHARPTR name)
{
    return strcmp("." , name) && strcmp("..", name);
}



/***********************************************************************
 * Function name : int FindNumOfDir(CHARPTR dirName)
 * 
 * Description   : find number of directories in dirName directory
 * 
 * Parameters    : directory name, number of directory pointer,
 *                 file pointer for directories and files, number of files,
 * 				   search target, output file
 *
 * Return        : void
************************************************************************/
void FindNumOfDir(CHARPTR dirName, INTPTR numOfDir,  FILEPTR dirFilePtr,
								   INTPTR numOfFile, FILEPTR filePtr,
								   CHARPTR target,   FILEPTR outPtr)
{
    DIRPTR dirPtr; 			/* directory object from opendir() 			*/
    struct dirent *direntp; /* information about next directory entry   */
    char path[DIRNAME_SIZ]; /* temporary char arrya for directory name  */ 	
		
	/* open the directroy */   
	dirPtr = opendir(dirName);

	/* opendir() status checked */
	if(dirPtr != NULL)
	{    		
		while( (direntp = readdir(dirPtr)) != NULL)
		{
			pathName(path,dirName,direntp->d_name);/*seth directory path name*/
            
			if( checkDirName(direntp->d_name) )
			{					
				if(!isdir(direntp) )
				{
					if( CheckPath(path)  == SUCCESS)
					{	
						StartGrep(path,target,outPtr);								       

						++(*numOfFile);
						fprintf(filePtr,"%s\n",path);		
					}
				}
				else{
						++(*numOfDir);						
						fprintf(dirFilePtr,"%s\n",path);
						
						FindNumOfDir(path,numOfDir,dirFilePtr, numOfFile,filePtr,target,outPtr);					
				}					
			}			
				
		}/*while end */
		
       closeDir(dirPtr); /* close directory */
	}/* if end */


}


/***********************************************************************
 * Function name : int CheckPath(CHARPTR path)
 * 
 * Description   : Control of path name
 * 
 * Parameters    : char pointer path name		   
 *
 * Return        : int, return status if there is '~' symbol in path name,
 * 					return 1,
 * 				   otherwise return 0
 * 
************************************************************************/
int CheckPath(CHARPTR path)
{
	int i=0;

	for(i=0; i< strlen(path) ; ++i)
		if(path[i] == '~')
			return ERROR;
			
	return SUCCESS;
}









/***********************************************************************
 * Function name : int isNumber(char ch)
 * 
 * Description   : Control of character is number?
 * 
 * Parameters    : a character		   
 *
 * Return        : int , return status if ch is number, return 1,
 * 				   otherwise return 0
 * 
************************************************************************/
int isNumber(char ch)
{
	int i;
	char nums[] = "0123456789";

	for(i=0; i < strlen(nums) ; ++i)
	{
		if(ch == nums[i])
			return SUCCESS;
	}
	return ERROR;
}





/***********************************************************************
 * Function name : pid_t Wait(int *stat_loc)
 * 
 * Description   : 
 * 
 * Parameters    : 		   
 *
 * Return        : 
 * 
************************************************************************/
pid_t Wait(int *stat_loc)
{
    int retVal;

    while( (retVal = wait(stat_loc)) == -1 && (errno == EINTR) );

    return retVal;
}


/***********************************************************************
 * Function name : pid_t Fork()
 * 
 * Description   : create fork
 * 
 * Parameters    : there is not any parameters	   
 *
 * Return        : return pid
 * 
************************************************************************/
pid_t Fork()
{
     pid_t pid;
     if( ( pid = fork() ) == -1 )
     {
        perror("Failed to fork :");
        exit(-1);
     }
     return pid;
}
