/*######################################################################
 * 
 * LECTURE NAME  :   BIL244 SYSTEM PROGRAMMING /  2014
 * 
 * HOMEWORK  2   :   ./bulBeni dirName -g "string" -l numOfLine
 * 
 * STUDENT       :   FATMA CELIK - 101044038
 *
 * DATE          :   18 MARCH 2014 / TUESDAY
 * 
######################################################################*/

#include "bulBeni.h"



/***********************************************************************
 * Function name : Search(CHARPTR str, CHARPTR target )
 * 
 * Description   : Search target and find number of target in str
 * 
 * Parameters    : two char array str and target			   
 *
 * Return        : int , return number of target in str
 * 
************************************************************************/
int Search(CHARPTR str, CHARPTR target )
{
	int count=0, status=0;
	int i;

	for(i=0; i< strlen(str) ; ++i)
	{
		status = 0;
		if( str[i] == target[0]) 
		{/* if target first character is found, call IsSubStr function*/
			status = IsSubStr(&str[i], target);

			if(status == 1)
				count++; /* if target is found, increase count*/
		}
	}

	return count;
}

/***********************************************************************
 * Function name : IsSubStr(CHARPTR str, CHARPTR target)
 * 
 * Description   : Control of target is sub string in str
 * 
 * Parameters    : two char array str and target			   
 *
 * Return        : int , return status if target is in str, return 1,
 * 				   otherwise return 0
 * 
************************************************************************/
int IsSubStr(CHARPTR str, CHARPTR target)
{
	int i;
	
	for( i=0; i<strlen(target); ++i)
	{
		if(target[i] != str[i])
			return 0;   /* letters not same, then target */
					    /*is not sub string, return false*/
	}


	return 1; /* if target is sub string of str, return true*/
}

/***********************************************************************
 * Function name : Free(CHARPTRPTR str ,int size)
 * 
 * Description   : free function for malloc 
 * 
 * Parameters    : 2D char array and size			   
 *
 * Return        : void
 * 
************************************************************************/
void Free(CHARPTRPTR str ,int size)
{
	int i;

	for(i=0; i< size; i++)
		free(str[i]);

	free(str);
}



/***********************************************************************
 * Function name : Start(CHARPTR fileName, CHARPTR target)
 * 
 * Description   : starting  all operation and call functions
 * 
 * Parameters    : input file name, searching array <target>		   
 *
 * Return        : void
 * 
************************************************************************/
void StartGrep(CHARPTR fileName, CHARPTR target, FILEPTR outPtr)
{
	/* variables */
    int numOfLine=0;
	INTPTR sizeOfLine;/* number of characters in a line in file*/

	/* number of lines in file*/
	numOfLine = FindNumberOfLine(fileName);

	if( numOfLine > 0)
	{
		/* malloc for read datas from file*/
		sizeOfLine = (int*)malloc(sizeof(int) * numOfLine);
		if(sizeOfLine != NULL)
		{
			/*find number of characters in file and */
			/*save numer of chars in integer array sizeOfLine*/	
			FindNumOfChars(fileName,sizeOfLine);
			GrepM(fileName,sizeOfLine,numOfLine,target, outPtr);
						
			free(sizeOfLine);		/*call free for malloc*/					
		}
		else
		{
			fprintf(stdout,"\n Malloc is failed in Start function..\n");
			free(sizeOfLine);		/*call free for malloc*/		
			exit(ERROR);
		}

	}
	
}



/***********************************************************************
 * Function name : FindNumberOfLine(CHARPTR fileName)
 * 
 * Description   : find number of line in input file
 * 
 * Parameters    : input file name	   
 *
 * Return        : int, return number of lines in file
 * 
************************************************************************/
int FindNumberOfLine(CHARPTR fileName)
{
    FILE *fPtr;
    char c = '0';
    int line = 0,count=0;

    if ((fPtr = fopen(fileName, "r")) == NULL)
    {		
        fprintf(stdout,"\n File not open in FindNumberOfLine function. \n");
        fclose(fPtr); /* close file */
		exit(ERROR);
    }
    else
    {       
		do{
			c = fgetc(fPtr); /* read character in file*/

			if(c == '\n')
				count++; /* increase line number*/
				
		}while(c != EOF);

		line = count; /* number of line in file*/

        fclose(fPtr); /* close file */
    }

    return line;
}


/***********************************************************************
 * Function name : FindNumOfChars(CHARPTR filename,INTPTR lines)
 * 
 * Description   : find number of characters in a line
 * 
 * Parameters    : input file name, integer array lines	   
 *
 * Return        : void
 * 
************************************************************************/
void FindNumOfChars(CHARPTR fileName,INTPTR lines)
{
    FILE *fPtr;
    char c;
    int line = 0,count=0;

    if ((fPtr = fopen(fileName, "r")) == NULL)
    {
		fprintf(stdout,"\n Not open file in FindNumOfChars fnction.\n");
		fclose(fPtr);/*close file*/
		exit(ERROR);
    }
    else
    {       
		do{
			c = fgetc(fPtr);
			count++;

			if(c == '\n')
			{
				/* save number of characters in a line*/
				lines[line]=count;
				line++;/* increase line index*/
				count=0;
			}
								
		}while(c != EOF);
		
		fclose(fPtr);/*close file*/
    }	 
}




/***********************************************************************
 * Function name : char  printLines(FILEPTR inputFile,int numOfLine)
 * 
 * Description   : print lines
 * 
 * Parameters    : input file name, integer number of line  
 *
 * Return        : ch
 * 
************************************************************************/
char  printLines(FILEPTR inputFile,int numOfLine)
{
    int i=0;
    char ch='-'; /* character of read in inputfile*/
       
    for(i=1; i <= numOfLine &&  NOT_EOF ; ++i){
    
        ch = fgetc(inputFile);
        
        while(  ch != '\n' && NOT_EOF )    
        {
            printf("%c",ch);  
            ch = fgetc(inputFile);              
        }    
        
        if( NOT_EOF )
			printf("\n");    
     }   
       
    return (char)ch;
}




/***********************************************************************
 * Function name : char getCh(void)
 * 
 * Description   : take a character from terminal
 * 
 * Parameters    : void
 *
 * Return        : a character from terminal
 * 
************************************************************************/ 
char getCh(void)
 {
    
    struct termios oldt, newt;    
    int ch;
    /* Initialize new terminal i/o settings */
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    
    /* get character */
    ch = getchar();
    
    /* Restore old terminal i/o settings */
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    
    return (char)ch;
}


/***********************************************************************
 * Function name : void startPrint(FILEPTR inputFile,int numOfLine)
 * 
 * Description   : starting lines on screen
 * 
 * Parameters    : input file name, integer number of line  
 *
 * Return        : void
 * 
************************************************************************/
void startPrint(FILEPTR inputFile,int numOfLine)
{
    char tempCh='c';    /* temporary variable for 'space' or 'enter' characters*/
    char ch = 'c';        /* character of last read  */
    int flag = 0;   /* status for invalid variable */
    
    /*call function, first print operation  */
    ch = printLines(inputFile,numOfLine);
    
    while( NOT_EOF && tempCh != 'q' ){
                  
        do{
            
            tempCh = getCh();                
			if(tempCh == '\n')
			{ /* checked enter */   
                ch = printLines(inputFile,numOfLine);
                flag = 0;
            
            }else if( tempCh == 'q')
			{
				flag = 0;
			}
            else
                flag = 1;         
            
        }while(flag);/* do-while end */
        
    }/* while end */

	if(tempCh != 'q')
		printf("\n");

}

static int x=0;
/***********************************************************************
 * Function name : GrepM(char *fileName, int *sizeOfLine,
 * 										 int lineLen, CHARPTR target)
 * 
 * Description   : Search target in file and save number of target in file 
 * 
 * Parameters    : input file name,
 * 				   number of characters in line(size of line),
 * 				   number of line in file,
 * 				   search string target			   
 *
 * Return        : void
 * 
************************************************************************/
void GrepM(CHARPTR fileName, INTPTR sizeOfLine, int numOfLine, CHARPTR target, FILEPTR outPtr)
{
	FILEPTR fPtr;
	CHARPTRPTR str;
	int i,count=0,sum=0;
	pid_t pID;
	++x;
	pID = getpid();
	
    if ((fPtr = fopen(fileName,"r")) == NULL )
    {
		fprintf(stdout,"\n Not open file. (Function name : GrepM)\n");
		fclose(fPtr); /* close file*/		
		exit(ERROR);
	}
    else
    {
		/* malloc for  2d char array*/
		str = (char**)malloc(sizeof(char*) * numOfLine);
		if( str != NULL)
		{		
			for(i=0; i<numOfLine;++i)
			{
				/*malloc to read string  for i. line*/
				str[i] = (char*)malloc(sizeof(char) * ( sizeOfLine[i] * 2) );
				if(str[i] != NULL )
				{
					/*read string  i. line */
					fgets(str[i], sizeof(char*) * (sizeOfLine[i] * 2), fPtr);
						
					/* count is number of target in str[i]*/
					/*search target in string str[i]*/
					count = Search(str[i],target);
					if(count !=0)
					{
						fprintf(outPtr," Pid: %d  File Name: %s: Line%3d:"
									   " Number of  \"%s\": %d \n",(int)(pID+x),fileName,i+1,target,count);
					}
					sum += count;/* sum of <number of target>*/
					count=0;					
					
				}/* end if str != null*/
				else
				{
					fprintf(stdout,"\n Malloc is failed in GrepM function.(str[%d])!! ",i);
					Free(str,i+1);
					fclose(fPtr); /* close file*/		
					exit(ERROR);
				}
			}/*end for*/

		}/*end if*/
		else
		{
			fprintf(stdout,"\n Malloc is failed in GrepM function.(str)!! ");
			free(str);
			fclose(fPtr); /* close file*/			
			exit(ERROR);
		}/*end else*/ 
		
		fclose(fPtr); /* close file*/
		Free(str,numOfLine);/* call free function for malloc*/
		
	}/*end else*/

}/*end function*/
