/*######################################################################
 * 
 * LECTURE NAME  :   BIL244 SYSTEM PROGRAMMING /  2014
 * 
 * HOMEWORK  2   :   ./bulBeni dirName -g "string" -l numOfLine
 * 
 * STUDENT       :   FATMA CELIK - 101044038
 *
 * DATE          :   18 MARCH 2014 / TUESDAY
 * 
######################################################################*/

#ifndef BULBENI_H
#define BULBENI_H

	/************************** INCLUDES *********************************/
	#include <stdio.h>   /* printf()           */
	#include <string.h>  /* strcmp(), strlen() */
	#include <stdlib.h>  /* malloc()           */
	#include <unistd.h>
	#include <errno.h>
	#include <dirent.h>
	#include <limits.h>
	#include <time.h>
	#include <sys/stat.h>
	#include <wait.h>
	#include <ctype.h>    
	#include <termios.h>
	#include <sys/types.h>
	#include <signal.h>
	/************************** DEFINES **********************************/

	#define ERROR 0
	#define NOT_OPEN -1
	#define SUCCESS 1
	#define CLEAR_SCREEN system("clear")
	#define DIRNAME_SIZ 500000

	#define DIRECTORIES  "directories.txt"
	#define FILES        "files.txt"
	#define OUTPUT       "screen.txt"

	#ifndef PATH_MAX
	#define PATH_MAX 500000
	#endif


	#define FAIL -1
	#define NOT_NUMBER -1
	#define NOT_FILE -1
	#define DEFAULT_NUM 24
	#define MIN_ARG_NUM 2   /* number of minumum argument */
	#define NOT_EOF ( (ch) != EOF )  

	
	/************************** TYPEDEFS *********************************/
	typedef int*   INTPTR; 		 /* integer pointer       */
	typedef char*  CHARPTR;      /* char pointer          */
	typedef char** CHARPTRPTR;   /* two dimensional array */
	typedef FILE*  FILEPTR;      /* FILE pointer          */
	typedef DIR*   DIRPTR;       /* DIRECTORY  pointer    */


	/************************** DIRECTORY FUNCTIONS ********************************/
	
	/* check of main arguments*/
	int Usage(int argc, CHARPTRPTR argv);
	void printMes(int argc, CHARPTRPTR argv);
	/* check of number characters*/
	int isNumber(char ch);
	/* program starts and call all other functions in this function*/
	void Start(CHARPTRPTR argv);
	/* find number of directories in dirName directory*/
	void FindNumOfDir(CHARPTR dirName, INTPTR numOfDir,  FILEPTR dirFilePtr,
				INTPTR numOfFile, FILEPTR filePtr,CHARPTR target,   FILEPTR outPtr);

	/* check is directory */
	int isdir( struct dirent *direntp );
	/* close directory*/
	void closeDir(DIRPTR dirPtr);
	/* set directory path name */
	void pathName(CHARPTR path ,CHARPTR dirName,CHARPTR name );	

	/*check path name*/
	int CheckPath(CHARPTR path);
	
	/************************** GREP FUNCTIONS ********************************/
	
	/* start to find target in files*/
	void StartGrep(CHARPTR fileName, CHARPTR target, FILEPTR outPtr);
	
	/* find number of lines in input file*/
	int  FindNumberOfLine(CHARPTR fileName);
	
	/*  check of target is sub string of str*/
	int  IsSubStr(CHARPTR str, CHARPTR target);
	
	/* search target in string str*/
	int  Search(CHARPTR str, CHARPTR target);
		
	/* find number of characters in a line in input file*/
	void FindNumOfChars(CHARPTR fileName, INTPTR  lines);
	
	/* grep function, call search function and search target in file*/
	void GrepM(CHARPTR fileName, INTPTR sizeOfLine, int numOfLine, CHARPTR target, FILEPTR outPtr);
	
	/* deallocate - free function for two dimensional array*/
	void Free(CHARPTRPTR str, int size);

	/*create fork*/
	pid_t Fork();
	pid_t Wait(int *stat_loc);

	/************************** GREP FUNCTIONS ********************************/
	/* printed */
	void startPrint(FILEPTR inputFile,int numOfLine);
	
	char printLines(FILEPTR inputFile,int numOfLine);
	/* take a command from terminal*/
	char getCh(void) ;
	
	
#endif

